class Person
{
	constructor(firstName, lastName)
	{
		this.firstName = firstName;
		this.lastName = lastName;
	}

	getFullName()
	{
		var fullName = this.lastName + ", " + this.firstName;

		return fullName;
	} 
}