class Student extends Person
{
	constructor(firstName, lastName, IDNumber, course)
	{
		super(firstName, lastName);

		this.IDNumber = IDNumber;
		this.course = course;
	}

	getIDNumber()
	{
		return this.IDNumber;
	}

	getCourse()
	{
		return this.course;
	}

	printStudentInfo()
	{
		var fullName = this.getFullName();

		console.log("Full Name: " + fullName);
		console.log("Given Name: " + this.firstName);
		console.log("Last Name: " + this.lastName);
		console.log("ID Number: " + this.getIDNumber());
		console.log("Course: " + this.getCourse());
	}
}