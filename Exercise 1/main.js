function getLargest(array) // Item # 1
{
	var largestNumber = array[0];

	for(let i = 1; i <= array.length; i++)
	{
		if(array[i] > largestNumber)
		{
			largestNumber = array[i];
		}
	}

	return largestNumber;
}

function getSum(array) // Item # 2
{
    var sum = 0;
        
    for(let i = 0; i < array.length; i++)
    {
        sum = sum + numbers[i];
    }

	return sum;
}

function getAverage(array) // Item # 3
{
	var total = 0;

    for(let i = 0; i < array.length; i++)
    {
        total = total + numbers[i];
    }

	var average = total / array.length;

	return average;
}

function degreesToRadians(angle) // Item # 4
{
	return angle * (Math.PI / 180);
}

function clamp(input, minimum, maximum) // Item # 5
{
	if(input <= minimum)
	{
		input = minimum;
	}

	else if(input >= maximum)
	{
		input = maximum;
	}

	return input;
}

function getEuclideanDistance(x1, x2, y1, y2) // Item # 6
{
	distance = Math.sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)));

	return distance;
}

function bubbleSorter(array) // Item # 7
{
	var tempNum;

	for (let i = 1; i <= array.length; i++)
	{
		for (let j = 0; j < (array.length - 1); j++)
		{
			if (array[j + 1] < array[j])
			{
				tempNum = array[j];
				array[j] = array[j + 1];
				array[j + 1] = tempNum;
			}
		}
	}

	return array;
}

function printArray(array)
{
	for(let i = 0; i < array.length; i++) 
	{
		console.log(array[i]);
	}
}

var Vector2 = function(x, y, magnitude)
{
	this.x = x;
	this.y = y;
	this.magnitude = magnitude;
}

var numbers = [16, 23, 2, 22, 17, 13];
var angle = 113;

var inputA = 98;
var inputB = 116;
var minimum = 99;
var maximum = 115;

var x1 = 1;
var x2 = 3;
var y1 = 9;
var y2 = 8;

printArray(numbers); // 16, 23, 2, 22, 17, 13

var largestNumber = getLargest(numbers);

console.log(largestNumber); // 23

var sum = getSum(numbers);

console.log(sum); // 93

var average = getAverage(numbers);

console.log(average); // 15.5

var radianAngle = degreesToRadians(angle);

console.log(radianAngle); // 1.9722220547535925...

var numberA = clamp(inputA, minimum, maximum);

console.log(numberA); // 99

var numberB = clamp(inputB, minimum, maximum);

console.log(numberB); // 115

var distance = getEuclideanDistance(x1, x2, y1, y2);

console.log(distance); // 2.23606797749979

bubbleSorter(numbers);

printArray(numbers); // 2, 13, 16, 17, 22, 23

var point = new Vector2(1, 13, 21);

console.log(point.x); // 1
console.log(point.y); // 13
console.log(point.magnitude); // 21

var personA = new Student("Jaynel", "Dugos", "11614162", "BSIEMC-GD");

personA.printStudentInfo();