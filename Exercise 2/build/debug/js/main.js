/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"main": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/index.js","vendor"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/Main.js":
/*!*********************!*\
  !*** ./src/Main.js ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\n\nvar _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();\n\nvar _assetdirectory = __webpack_require__(/*! ./assetdirectory */ \"./src/assetdirectory.js\");\n\nvar _pixi = __webpack_require__(/*! pixi.js */ \"./node_modules/pixi.js/lib/index.js\");\n\nvar _TitleScreen = __webpack_require__(/*! ./screen/TitleScreen */ \"./src/screen/TitleScreen.js\");\n\nvar _TitleScreen2 = _interopRequireDefault(_TitleScreen);\n\nvar _ = __webpack_require__(/*! . */ \"./src/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nvar Main = function () {\n    function Main() {\n        _classCallCheck(this, Main);\n    }\n\n    _createClass(Main, null, [{\n        key: \"start\",\n        value: function start() {\n            Main.cjAudioQueue = new createjs.LoadQueue();\n            createjs.Sound.alternateExtensions = [\"ogg\"];\n\n            Main.cjAudioQueue.installPlugin(createjs.Sound);\n            Main.cjAudioQueue.addEventListener(\"complete\", Main.handleAudioComplete.bind(Main));\n\n            if (_assetdirectory.AssetDirectory.audio.length > 0) {\n                //LOAD AUDIO                  \n                var audioFiles = _assetdirectory.AssetDirectory.audio;\n                var audioManifest = [];\n                for (var i = 0; i < audioFiles.length; i++) {\n                    audioManifest.push({\n                        id: audioFiles[i],\n                        src: audioFiles[i]\n                    });\n                }\n                Main.cjAudioQueue.loadManifest(audioManifest);\n            } else {\n                Main.handleAudioComplete();\n            }\n        }\n    }, {\n        key: \"handleAudioComplete\",\n        value: function handleAudioComplete() {\n            if (_assetdirectory.AssetDirectory.load.length > 0) {\n                //LOAD IMAGES         \n                var loader = _pixi.loaders.shared;\n                loader.add(_assetdirectory.AssetDirectory.load);\n                loader.load(Main.handleImageComplete);\n            } else {\n                Main.handleImageComplete();\n            }\n        }\n    }, {\n        key: \"handleImageComplete\",\n        value: function handleImageComplete() {\n            var screen = new _TitleScreen2.default();\n            _.App.stage.addChild(screen);\n        }\n    }]);\n\n    return Main;\n}();\n\nexports.default = Main;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvTWFpbi5qcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9zcmMvTWFpbi5qcz8xMjIxIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEFzc2V0RGlyZWN0b3J5IH0gZnJvbSBcIi4vYXNzZXRkaXJlY3RvcnlcIjtcclxuaW1wb3J0IHsgbG9hZGVycyB9IGZyb20gXCJwaXhpLmpzXCI7XHJcbmltcG9ydCBUaXRsZVNjcmVlbiBmcm9tIFwiLi9zY3JlZW4vVGl0bGVTY3JlZW5cIjtcclxuaW1wb3J0IHsgQXBwIH0gZnJvbSBcIi5cIjtcclxuXHJcbmNsYXNzIE1haW4gXHJcbntcclxuICAgIHN0YXRpYyBzdGFydCgpXHJcbiAgICB7XHJcbiAgICAgICAgTWFpbi5jakF1ZGlvUXVldWUgPSBuZXcgY3JlYXRlanMuTG9hZFF1ZXVlKCk7XHJcbiAgICAgICAgY3JlYXRlanMuU291bmQuYWx0ZXJuYXRlRXh0ZW5zaW9ucyA9IFtcIm9nZ1wiXTtcclxuXHJcbiAgICAgICAgTWFpbi5jakF1ZGlvUXVldWUuaW5zdGFsbFBsdWdpbihjcmVhdGVqcy5Tb3VuZCk7XHJcbiAgICAgICAgTWFpbi5jakF1ZGlvUXVldWUuYWRkRXZlbnRMaXN0ZW5lcihcImNvbXBsZXRlXCIsIE1haW4uaGFuZGxlQXVkaW9Db21wbGV0ZS5iaW5kKE1haW4pKTtcclxuXHJcbiAgICAgICAgaWYoQXNzZXREaXJlY3RvcnkuYXVkaW8ubGVuZ3RoID4gMClcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIC8vTE9BRCBBVURJTyAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBsZXQgYXVkaW9GaWxlcyA9IEFzc2V0RGlyZWN0b3J5LmF1ZGlvO1xyXG4gICAgICAgICAgICBsZXQgYXVkaW9NYW5pZmVzdCA9IFtdO1xyXG4gICAgICAgICAgICBmb3IobGV0IGkgPSAwOyBpIDwgYXVkaW9GaWxlcy5sZW5ndGg7IGkrKylcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgYXVkaW9NYW5pZmVzdC5wdXNoXHJcbiAgICAgICAgICAgICAgICAoXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZDogYXVkaW9GaWxlc1tpXSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc3JjOiBhdWRpb0ZpbGVzW2ldXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBNYWluLmNqQXVkaW9RdWV1ZS5sb2FkTWFuaWZlc3QoYXVkaW9NYW5pZmVzdCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBlbHNlXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBNYWluLmhhbmRsZUF1ZGlvQ29tcGxldGUoKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBoYW5kbGVBdWRpb0NvbXBsZXRlKClcclxuICAgIHtcclxuICAgICAgICBpZihBc3NldERpcmVjdG9yeS5sb2FkLmxlbmd0aCA+IDApXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICAvL0xPQUQgSU1BR0VTICAgICAgICAgXHJcbiAgICAgICAgICAgIGxldCBsb2FkZXIgPSBsb2FkZXJzLnNoYXJlZDtcclxuICAgICAgICAgICAgbG9hZGVyLmFkZChBc3NldERpcmVjdG9yeS5sb2FkKTtcclxuICAgICAgICAgICAgbG9hZGVyLmxvYWQoTWFpbi5oYW5kbGVJbWFnZUNvbXBsZXRlKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSBcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIE1haW4uaGFuZGxlSW1hZ2VDb21wbGV0ZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgaGFuZGxlSW1hZ2VDb21wbGV0ZSgpXHJcbiAgICB7XHJcbiAgICAgICAgbGV0IHNjcmVlbiA9IG5ldyBUaXRsZVNjcmVlbigpO1xyXG4gICAgICAgIEFwcC5zdGFnZS5hZGRDaGlsZChzY3JlZW4pO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBNYWluOyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7OztBQUFBO0FBQ0E7Ozs7O0FBQ0E7Ozs7Ozs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUdBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFDQTtBQUlBO0FBQ0E7QUFFQTs7O0FBR0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFHQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/Main.js\n");

/***/ }),

/***/ "./src/assetdirectory.js":
/*!*******************************!*\
  !*** ./src/assetdirectory.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n\tvalue: true\n});\nvar AssetDirectory = exports.AssetDirectory = {\n\t\"load\": [\"assets/fonts/Monofur.eot\", \"assets/fonts/Monofur.svg\", \"assets/fonts/Monofur.ttf\", \"assets/fonts/Monofur.woff\", \"assets/fonts/Monofur.woff2\", \"assets/fonts/stylesheet.css\"],\n\t\"audio\": []\n};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXNzZXRkaXJlY3RvcnkuanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL2Fzc2V0ZGlyZWN0b3J5LmpzPzZmZDEiXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGxldCBBc3NldERpcmVjdG9yeSA9IHtcblx0XCJsb2FkXCI6IFtcblx0XHRcImFzc2V0cy9mb250cy9Nb25vZnVyLmVvdFwiLFxuXHRcdFwiYXNzZXRzL2ZvbnRzL01vbm9mdXIuc3ZnXCIsXG5cdFx0XCJhc3NldHMvZm9udHMvTW9ub2Z1ci50dGZcIixcblx0XHRcImFzc2V0cy9mb250cy9Nb25vZnVyLndvZmZcIixcblx0XHRcImFzc2V0cy9mb250cy9Nb25vZnVyLndvZmYyXCIsXG5cdFx0XCJhc3NldHMvZm9udHMvc3R5bGVzaGVldC5jc3NcIlxuXHRdLFxuXHRcImF1ZGlvXCI6IFtdXG59OyJdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQTtBQUNBO0FBUUE7QUFUQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/assetdirectory.js\n");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\nexports.App = undefined;\n\nvar _assetdirectory = __webpack_require__(/*! ./assetdirectory.js */ \"./src/assetdirectory.js\");\n\nvar _pixi = __webpack_require__(/*! pixi.js */ \"./node_modules/pixi.js/lib/index.js\");\n\nvar _Main = __webpack_require__(/*! ./Main.js */ \"./src/Main.js\");\n\nvar _Main2 = _interopRequireDefault(_Main);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nvar Config = __webpack_require__(/*! Config */ \"Config\");\n\nvar canvas = document.getElementById('game-canvas');\nvar pixiapp = new _pixi.Application({\n    view: canvas,\n    width: Config.BUILD.WIDTH,\n    height: Config.BUILD.HEIGHT\n});\n\ndocument.body.style.margin = \"0px\";\ndocument.body.style.overflow = \"hidden\";\n\n/*****************************************\r\n ************* ENTRY POINT ***************\r\n *****************************************/\nfunction ready(fn) {\n    if (document.readyState != 'loading') {\n        fn();\n    } else {\n\n        document.addEventListener('DOMContentLoaded', fn);\n    }\n}\n\nready(function () {\n    _Main2.default.start();\n});\n\nexports.App = pixiapp;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvaW5kZXguanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL2luZGV4LmpzPzEyZDUiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQXNzZXREaXJlY3RvcnkgfSBmcm9tICcuL2Fzc2V0ZGlyZWN0b3J5LmpzJztcclxuaW1wb3J0IHsgQXBwbGljYXRpb24gfSBmcm9tICdwaXhpLmpzJztcclxuaW1wb3J0IE1haW4gZnJvbSAnLi9NYWluLmpzJztcclxuXHJcbnZhciBDb25maWcgPSByZXF1aXJlKCdDb25maWcnKTtcclxuXHJcbmxldCBjYW52YXMgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnZ2FtZS1jYW52YXMnKTtcclxubGV0IHBpeGlhcHAgPSBuZXcgQXBwbGljYXRpb25cclxuKFxyXG4gICAge1xyXG4gICAgICAgIHZpZXc6IGNhbnZhcyxcclxuICAgICAgICB3aWR0aDogQ29uZmlnLkJVSUxELldJRFRILFxyXG4gICAgICAgIGhlaWdodDogQ29uZmlnLkJVSUxELkhFSUdIVFxyXG4gICAgfVxyXG4pXHJcblxyXG5kb2N1bWVudC5ib2R5LnN0eWxlLm1hcmdpbiA9IFwiMHB4XCI7XHJcbmRvY3VtZW50LmJvZHkuc3R5bGUub3ZlcmZsb3cgPSBcImhpZGRlblwiO1xyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcbiAqKioqKioqKioqKioqIEVOVFJZIFBPSU5UICoqKioqKioqKioqKioqKlxyXG4gKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbmZ1bmN0aW9uIHJlYWR5KGZuKSBcclxue1xyXG4gICAgaWYgKGRvY3VtZW50LnJlYWR5U3RhdGUgIT0gJ2xvYWRpbmcnKSBcclxuICAgIHtcclxuICAgICAgICBmbigpO1xyXG4gICAgfSBcclxuICAgIFxyXG4gICAgZWxzZSBcclxuICAgIHtcclxuXHJcbiAgICAgICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignRE9NQ29udGVudExvYWRlZCcsIGZuKTtcclxuICAgIH1cclxufVxyXG5cclxucmVhZHkoZnVuY3Rpb24oKSBcclxue1xyXG4gICAgTWFpbi5zdGFydCgpO1xyXG59KTtcclxuXHJcbmV4cG9ydCB7cGl4aWFwcCBhcyBBcHB9OyJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTs7Ozs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUhBO0FBQ0E7QUFNQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/index.js\n");

/***/ }),

/***/ "./src/screen/TitleScreen.js":
/*!***********************************!*\
  !*** ./src/screen/TitleScreen.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n        value: true\n});\n\nvar _pixi = __webpack_require__(/*! pixi.js */ \"./node_modules/pixi.js/lib/index.js\");\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\"); } return call && (typeof call === \"object\" || typeof call === \"function\") ? call : self; }\n\nfunction _inherits(subClass, superClass) { if (typeof superClass !== \"function\" && superClass !== null) { throw new TypeError(\"Super expression must either be null or a function, not \" + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }\n\nvar TitleScreen = function (_Container) {\n        _inherits(TitleScreen, _Container);\n\n        function TitleScreen() {\n                _classCallCheck(this, TitleScreen);\n\n                var _this = _possibleConstructorReturn(this, (TitleScreen.__proto__ || Object.getPrototypeOf(TitleScreen)).call(this));\n\n                var filledCircle = new PIXI.Graphics();\n                var filledRectangle = new PIXI.Graphics();\n                var filledEllipse = new PIXI.Graphics();\n                var outlinedCircle = new PIXI.Graphics();\n                var outlinedRectangle = new PIXI.Graphics();\n                var outlinedEllipse = new PIXI.Graphics();\n                var box = new PIXI.Graphics();\n                var sideB = new PIXI.Graphics();\n                var sideC = new PIXI.Graphics();\n                var sideD = new PIXI.Graphics();\n                var star = new PIXI.Graphics();\n\n                filledCircle.beginFill(0X98d8d8);\n                filledCircle.drawCircle(50, 60, 45);\n                filledCircle.endFill();\n\n                filledRectangle.beginFill(0Xc03028);\n                filledRectangle.drawRect(100, 100, 175, 210);\n                filledRectangle.endFill();\n\n                filledEllipse.beginFill(0X78c850);\n                filledEllipse.drawEllipse(200, 400, 180, 75);\n                filledEllipse.endFill();\n\n                outlinedCircle.lineStyle(10, 0X98d8d8);\n                outlinedCircle.drawCircle(50, 60, 45);\n\n                outlinedRectangle.lineStyle(10, 0Xc03028);\n                outlinedRectangle.drawRect(100, 100, 175, 210);\n\n                outlinedEllipse.lineStyle(10, 0X78c850);\n                outlinedEllipse.drawEllipse(200, 400, 180, 75);\n\n                box.lineStyle(10, 0XFFFFFF);\n                box.moveTo(100, 100);\n                box.lineTo(100, 200);\n                box.lineTo(150, 200);\n                box.lineTo(150, 100);\n                box.lineTo(100, 100);\n\n                star.beginFill(0Xf8d030);\n                star.drawStar(500, 300, 5, 100, 50, 0);\n                star.endFill();\n\n                var IDNumber = new _pixi.Text(\"11614162\", {\n                        fill: \"White\",\n                        fontSize: 18,\n                        fontFamily: 'Monofur',\n                        wordWrap: false,\n                        align: 'Left'\n                });\n\n                var fullName = new _pixi.Text(\"Jaynel P. Dugos\", {\n                        fill: [0Xa040a0, 0X78c850, 0X6890f0, 0Xf8d030],\n                        fontSize: 20,\n                        fontFamily: 'Monofur',\n                        wordWrap: false,\n                        align: 'Left',\n                        fillGradientType: _pixi.TEXT_GRADIENT.LINEAR_HORIZONTAL\n                });\n\n                var courseName = new _pixi.Text(\"Game Development for the Web\", {\n                        fill: [0Xf8d030, 0Xb8b8d0],\n                        fontSize: 18,\n                        fontFamily: 'Monofur',\n                        wordWrap: false,\n                        align: 'Left',\n                        fillGradientType: _pixi.TEXT_GRADIENT.LINEAR_VERTICAL,\n                        fillGradientStops: [0.5, 0.5]\n                });\n\n                var programName = new _pixi.Text(\"Bachelor of Science in Interactive Entertaiment and Multimedia Computing major in Game Development\", {\n                        fill: [0X8c850, 0X08030, 0X890f0, 0X8d030],\n                        fontSize: 18,\n                        fontFamily: 'Monofur',\n                        wordWrap: true,\n                        wordWrapWidth: 150,\n                        align: 'Left',\n                        fillGradientType: _pixi.TEXT_GRADIENT.LINEAR_VERTICAL,\n                        fillGradientStops: [0.5, 0.5]\n                });\n\n                var IGN = new _pixi.Text(\"My IGN: Zeineru Hachimitsu\", {\n                        fill: [0Xa8b820, 0X890f0],\n                        fontSize: 22,\n                        fontFamily: 'Monofur',\n                        wordWrap: false,\n                        align: 'Left',\n                        fillGradientType: _pixi.TEXT_GRADIENT.LINEAR_HORIZONTAL,\n                        fillGradientStops: [0.3, 0.7]\n                });\n\n                _this.addChild(filledCircle);\n                _this.addChild(filledRectangle);\n                _this.addChild(filledEllipse);\n\n                setTimeout(function () {\n                        _this.removeChild(filledCircle);\n                        _this.removeChild(filledRectangle);\n                        _this.removeChild(filledEllipse);\n\n                        _this.addChild(outlinedCircle);\n                        _this.addChild(outlinedRectangle);\n                        _this.addChild(outlinedEllipse);\n                }, 1500);\n\n                setTimeout(function () {\n                        _this.removeChild(outlinedCircle);\n                        _this.removeChild(outlinedRectangle);\n                        _this.removeChild(outlinedEllipse);\n\n                        _this.addChild(box);\n                        _this.addChild(sideB);\n                        _this.addChild(sideC);\n                        _this.addChild(sideD);\n                        _this.addChild(star);\n                }, 3000);\n\n                setTimeout(function () {\n                        _this.removeChild(box);\n                        _this.removeChild(sideB);\n                        _this.removeChild(sideC);\n                        _this.removeChild(sideD);\n                        _this.removeChild(star);\n\n                        _this.addChild(IDNumber);\n                }, 4500);\n\n                setTimeout(function () {\n                        _this.removeChild(IDNumber);\n\n                        _this.addChild(fullName);\n                }, 6500);\n\n                setTimeout(function () {\n                        _this.removeChild(fullName);\n\n                        _this.addChild(programName);\n                }, 8500);\n\n                setTimeout(function () {\n                        _this.removeChild(programName);\n\n                        _this.addChild(courseName);\n                }, 10500);\n\n                setTimeout(function () {\n                        _this.removeChild(courseName);\n\n                        _this.addChild(IGN);\n                }, 12500);\n                return _this;\n        }\n\n        return TitleScreen;\n}(_pixi.Container);\n\nexports.default = TitleScreen;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvc2NyZWVuL1RpdGxlU2NyZWVuLmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL3NyYy9zY3JlZW4vVGl0bGVTY3JlZW4uanM/YTM3MiJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb250YWluZXIsIFRleHQsIFRFWFRfR1JBRElFTlQsIENhbnZhc1JlbmRlcmVyIH0gZnJvbSBcInBpeGkuanNcIjtcclxuXHJcbmNsYXNzIFRpdGxlU2NyZWVuIGV4dGVuZHMgQ29udGFpbmVyXHJcbntcclxuICAgIGNvbnN0cnVjdG9yKClcclxuICAgIHtcclxuICAgICAgICBzdXBlcigpO1xyXG5cclxuICAgICAgICB2YXIgZmlsbGVkQ2lyY2xlID0gbmV3IFBJWEkuR3JhcGhpY3MoKTtcclxuICAgICAgICB2YXIgZmlsbGVkUmVjdGFuZ2xlID0gbmV3IFBJWEkuR3JhcGhpY3MoKTtcclxuICAgICAgICB2YXIgZmlsbGVkRWxsaXBzZSA9IG5ldyBQSVhJLkdyYXBoaWNzKCk7XHJcbiAgICAgICAgdmFyIG91dGxpbmVkQ2lyY2xlID0gbmV3IFBJWEkuR3JhcGhpY3MoKTtcclxuICAgICAgICB2YXIgb3V0bGluZWRSZWN0YW5nbGUgPSBuZXcgUElYSS5HcmFwaGljcygpO1xyXG4gICAgICAgIHZhciBvdXRsaW5lZEVsbGlwc2UgPSBuZXcgUElYSS5HcmFwaGljcygpO1xyXG4gICAgICAgIHZhciBib3ggPSBuZXcgUElYSS5HcmFwaGljcygpO1xyXG4gICAgICAgIHZhciBzaWRlQiA9IG5ldyBQSVhJLkdyYXBoaWNzKCk7XHJcbiAgICAgICAgdmFyIHNpZGVDID0gbmV3IFBJWEkuR3JhcGhpY3MoKTtcclxuICAgICAgICB2YXIgc2lkZUQgPSBuZXcgUElYSS5HcmFwaGljcygpO1xyXG4gICAgICAgIHZhciBzdGFyID0gbmV3IFBJWEkuR3JhcGhpY3MoKTsgXHJcblxyXG4gICAgICAgIGZpbGxlZENpcmNsZS5iZWdpbkZpbGwoMFg5OGQ4ZDgpO1xyXG4gICAgICAgIGZpbGxlZENpcmNsZS5kcmF3Q2lyY2xlKDUwLCA2MCwgNDUpO1xyXG4gICAgICAgIGZpbGxlZENpcmNsZS5lbmRGaWxsKCk7XHJcblxyXG4gICAgICAgIGZpbGxlZFJlY3RhbmdsZS5iZWdpbkZpbGwoMFhjMDMwMjgpO1xyXG4gICAgICAgIGZpbGxlZFJlY3RhbmdsZS5kcmF3UmVjdCgxMDAsIDEwMCwgMTc1LCAyMTApO1xyXG4gICAgICAgIGZpbGxlZFJlY3RhbmdsZS5lbmRGaWxsKCk7XHJcblxyXG4gICAgICAgIGZpbGxlZEVsbGlwc2UuYmVnaW5GaWxsKDBYNzhjODUwKTtcclxuICAgICAgICBmaWxsZWRFbGxpcHNlLmRyYXdFbGxpcHNlKDIwMCwgNDAwLCAxODAsIDc1KTtcclxuICAgICAgICBmaWxsZWRFbGxpcHNlLmVuZEZpbGwoKTtcclxuXHJcbiAgICAgICAgb3V0bGluZWRDaXJjbGUubGluZVN0eWxlKDEwLCAwWDk4ZDhkOCk7XHJcbiAgICAgICAgb3V0bGluZWRDaXJjbGUuZHJhd0NpcmNsZSg1MCwgNjAsIDQ1KTtcclxuXHJcbiAgICAgICAgb3V0bGluZWRSZWN0YW5nbGUubGluZVN0eWxlKDEwLCAwWGMwMzAyOCk7XHJcbiAgICAgICAgb3V0bGluZWRSZWN0YW5nbGUuZHJhd1JlY3QoMTAwLCAxMDAsIDE3NSwgMjEwKTtcclxuXHJcbiAgICAgICAgb3V0bGluZWRFbGxpcHNlLmxpbmVTdHlsZSgxMCwgMFg3OGM4NTApO1xyXG4gICAgICAgIG91dGxpbmVkRWxsaXBzZS5kcmF3RWxsaXBzZSgyMDAsIDQwMCwgMTgwLCA3NSk7XHJcblxyXG4gICAgICAgIGJveC5saW5lU3R5bGUoMTAsIDBYRkZGRkZGKTtcclxuICAgICAgICBib3gubW92ZVRvKDEwMCwgMTAwKTtcclxuICAgICAgICBib3gubGluZVRvKDEwMCwgMjAwKTtcclxuICAgICAgICBib3gubGluZVRvKDE1MCwgMjAwKTtcclxuICAgICAgICBib3gubGluZVRvKDE1MCwgMTAwKTtcclxuICAgICAgICBib3gubGluZVRvKDEwMCwgMTAwKTtcclxuXHJcbiAgICAgICAgc3Rhci5iZWdpbkZpbGwoMFhmOGQwMzApO1xyXG4gICAgICAgIHN0YXIuZHJhd1N0YXIoNTAwLCAzMDAsIDUsIDEwMCwgNTAsIDApO1xyXG4gICAgICAgIHN0YXIuZW5kRmlsbCgpO1xyXG5cclxuICAgICAgICB2YXIgSUROdW1iZXIgPSBuZXcgVGV4dChcIjExNjE0MTYyXCIsIFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgZmlsbDogXCJXaGl0ZVwiLFxyXG4gICAgICAgICAgICBmb250U2l6ZTogMTgsXHJcbiAgICAgICAgICAgIGZvbnRGYW1pbHk6ICdNb25vZnVyJyxcclxuICAgICAgICAgICAgd29yZFdyYXA6IGZhbHNlLFxyXG4gICAgICAgICAgICBhbGlnbjogJ0xlZnQnXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHZhciBmdWxsTmFtZSA9IG5ldyBUZXh0KFwiSmF5bmVsIFAuIER1Z29zXCIsIFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgZmlsbDogWzBYYTA0MGEwLCAwWDc4Yzg1MCwgMFg2ODkwZjAsIDBYZjhkMDMwXSxcclxuICAgICAgICAgICAgZm9udFNpemU6IDIwLFxyXG4gICAgICAgICAgICBmb250RmFtaWx5OiAnTW9ub2Z1cicsXHJcbiAgICAgICAgICAgIHdvcmRXcmFwOiBmYWxzZSxcclxuICAgICAgICAgICAgYWxpZ246ICdMZWZ0JyxcclxuICAgICAgICAgICAgZmlsbEdyYWRpZW50VHlwZTogVEVYVF9HUkFESUVOVC5MSU5FQVJfSE9SSVpPTlRBTFxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICB2YXIgY291cnNlTmFtZSA9IG5ldyBUZXh0KFwiR2FtZSBEZXZlbG9wbWVudCBmb3IgdGhlIFdlYlwiLCBcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIGZpbGw6IFswWGY4ZDAzMCwgMFhiOGI4ZDBdLFxyXG4gICAgICAgICAgICBmb250U2l6ZTogMTgsXHJcbiAgICAgICAgICAgIGZvbnRGYW1pbHk6ICdNb25vZnVyJyxcclxuICAgICAgICAgICAgd29yZFdyYXA6IGZhbHNlLFxyXG4gICAgICAgICAgICBhbGlnbjogJ0xlZnQnLFxyXG4gICAgICAgICAgICBmaWxsR3JhZGllbnRUeXBlOiBURVhUX0dSQURJRU5ULkxJTkVBUl9WRVJUSUNBTCxcclxuICAgICAgICAgICAgZmlsbEdyYWRpZW50U3RvcHM6IFswLjUsIDAuNV1cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgdmFyIHByb2dyYW1OYW1lID0gbmV3IFRleHQoXCJCYWNoZWxvciBvZiBTY2llbmNlIGluIEludGVyYWN0aXZlIEVudGVydGFpbWVudCBhbmQgTXVsdGltZWRpYSBDb21wdXRpbmcgbWFqb3IgaW4gR2FtZSBEZXZlbG9wbWVudFwiLCBcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIGZpbGw6IFswWDhjODUwLCAwWDA4MDMwLCAwWDg5MGYwLCAwWDhkMDMwXSxcclxuICAgICAgICAgICAgZm9udFNpemU6IDE4LFxyXG4gICAgICAgICAgICBmb250RmFtaWx5OiAnTW9ub2Z1cicsXHJcbiAgICAgICAgICAgIHdvcmRXcmFwOiB0cnVlLFxyXG4gICAgICAgICAgICB3b3JkV3JhcFdpZHRoOiAxNTAsXHJcbiAgICAgICAgICAgIGFsaWduOiAnTGVmdCcsXHJcbiAgICAgICAgICAgIGZpbGxHcmFkaWVudFR5cGU6IFRFWFRfR1JBRElFTlQuTElORUFSX1ZFUlRJQ0FMLFxyXG4gICAgICAgICAgICBmaWxsR3JhZGllbnRTdG9wczogWzAuNSwgMC41XVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICB2YXIgSUdOID0gbmV3IFRleHQoXCJNeSBJR046IFplaW5lcnUgSGFjaGltaXRzdVwiLCBcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIGZpbGw6IFswWGE4YjgyMCwgMFg4OTBmMF0sXHJcbiAgICAgICAgICAgIGZvbnRTaXplOiAyMixcclxuICAgICAgICAgICAgZm9udEZhbWlseTogJ01vbm9mdXInLFxyXG4gICAgICAgICAgICB3b3JkV3JhcDogZmFsc2UsXHJcbiAgICAgICAgICAgIGFsaWduOiAnTGVmdCcsXHJcbiAgICAgICAgICAgIGZpbGxHcmFkaWVudFR5cGU6IFRFWFRfR1JBRElFTlQuTElORUFSX0hPUklaT05UQUwsXHJcbiAgICAgICAgICAgIGZpbGxHcmFkaWVudFN0b3BzOiBbMC4zLCAwLjddXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHRoaXMuYWRkQ2hpbGQoZmlsbGVkQ2lyY2xlKTtcclxuICAgICAgICB0aGlzLmFkZENoaWxkKGZpbGxlZFJlY3RhbmdsZSk7XHJcbiAgICAgICAgdGhpcy5hZGRDaGlsZChmaWxsZWRFbGxpcHNlKTtcclxuICAgICAgICBcclxuICAgICAgICBzZXRUaW1lb3V0KCgpPT5cclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRoaXMucmVtb3ZlQ2hpbGQoZmlsbGVkQ2lyY2xlKTtcclxuICAgICAgICAgICAgdGhpcy5yZW1vdmVDaGlsZChmaWxsZWRSZWN0YW5nbGUpO1xyXG4gICAgICAgICAgICB0aGlzLnJlbW92ZUNoaWxkKGZpbGxlZEVsbGlwc2UpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5hZGRDaGlsZChvdXRsaW5lZENpcmNsZSk7XHJcbiAgICAgICAgICAgIHRoaXMuYWRkQ2hpbGQob3V0bGluZWRSZWN0YW5nbGUpO1xyXG4gICAgICAgICAgICB0aGlzLmFkZENoaWxkKG91dGxpbmVkRWxsaXBzZSk7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgIH0sIDE1MDApO1xyXG5cclxuICAgICAgICBzZXRUaW1lb3V0KCgpPT5cclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRoaXMucmVtb3ZlQ2hpbGQob3V0bGluZWRDaXJjbGUpO1xyXG4gICAgICAgICAgICB0aGlzLnJlbW92ZUNoaWxkKG91dGxpbmVkUmVjdGFuZ2xlKTtcclxuICAgICAgICAgICAgdGhpcy5yZW1vdmVDaGlsZChvdXRsaW5lZEVsbGlwc2UpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5hZGRDaGlsZChib3gpO1xyXG4gICAgICAgICAgICB0aGlzLmFkZENoaWxkKHNpZGVCKTtcclxuICAgICAgICAgICAgdGhpcy5hZGRDaGlsZChzaWRlQyk7XHJcbiAgICAgICAgICAgIHRoaXMuYWRkQ2hpbGQoc2lkZUQpO1xyXG4gICAgICAgICAgICB0aGlzLmFkZENoaWxkKHN0YXIpO1xyXG4gICAgICAgIH0sIDMwMDApO1xyXG5cclxuICAgICAgICBzZXRUaW1lb3V0KCgpPT5cclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRoaXMucmVtb3ZlQ2hpbGQoYm94KTtcclxuICAgICAgICAgICAgdGhpcy5yZW1vdmVDaGlsZChzaWRlQik7XHJcbiAgICAgICAgICAgIHRoaXMucmVtb3ZlQ2hpbGQoc2lkZUMpO1xyXG4gICAgICAgICAgICB0aGlzLnJlbW92ZUNoaWxkKHNpZGVEKTtcclxuICAgICAgICAgICAgdGhpcy5yZW1vdmVDaGlsZChzdGFyKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuYWRkQ2hpbGQoSUROdW1iZXIpO1xyXG4gICAgICAgIH0sIDQ1MDApO1xyXG5cclxuICAgICAgICBzZXRUaW1lb3V0KCgpPT5cclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRoaXMucmVtb3ZlQ2hpbGQoSUROdW1iZXIpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5hZGRDaGlsZChmdWxsTmFtZSk7XHJcbiAgICAgICAgfSwgNjUwMCk7XHJcblxyXG4gICAgICAgIHNldFRpbWVvdXQoKCk9PlxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgdGhpcy5yZW1vdmVDaGlsZChmdWxsTmFtZSk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLmFkZENoaWxkKHByb2dyYW1OYW1lKTtcclxuICAgICAgICB9LCA4NTAwKTtcclxuXHJcbiAgICAgICAgc2V0VGltZW91dCgoKT0+XHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICB0aGlzLnJlbW92ZUNoaWxkKHByb2dyYW1OYW1lKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuYWRkQ2hpbGQoY291cnNlTmFtZSk7XHJcbiAgICAgICAgfSwgMTA1MDApO1xyXG5cclxuICAgICAgICBzZXRUaW1lb3V0KCgpPT5cclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRoaXMucmVtb3ZlQ2hpbGQoY291cnNlTmFtZSk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLmFkZENoaWxkKElHTik7XHJcbiAgICAgICAgfSwgMTI1MDApO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBUaXRsZVNjcmVlbjsiXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBO0FBQ0E7Ozs7Ozs7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUNBO0FBT0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQUNBO0FBUUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBQ0E7QUFTQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFSQTtBQUNBO0FBVUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBQ0E7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBdEtBO0FBdUtBO0FBQ0E7O0FBM0tBO0FBQ0E7QUE0S0EiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/screen/TitleScreen.js\n");

/***/ }),

/***/ "Config":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** external "{\"BUILD\":{\"GAME_TITLE\":\"GAME TITLE\",\"VERSION\":\"0.0.1\",\"WIDTH\":889,\"HEIGHT\":500,\"MINIFIED_EXTERNAL_JS\":false,\"MINIFIED_EXTERNAL_JS_PATH\":\"./src/external/minified\",\"UNMINIFIED_EXTERNAL_JS_PATH\":\"./src/external/unminified\",\"EXTERNAL_JS\":[\"soundjs.js\",\"preloadjs.js\"],\"ASSETPATH\":{\"load\":\"assets\",\"audio\":\"assets/audio\"}}}" ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = {\"BUILD\":{\"GAME_TITLE\":\"GAME TITLE\",\"VERSION\":\"0.0.1\",\"WIDTH\":889,\"HEIGHT\":500,\"MINIFIED_EXTERNAL_JS\":false,\"MINIFIED_EXTERNAL_JS_PATH\":\"./src/external/minified\",\"UNMINIFIED_EXTERNAL_JS_PATH\":\"./src/external/unminified\",\"EXTERNAL_JS\":[\"soundjs.js\",\"preloadjs.js\"],\"ASSETPATH\":{\"load\":\"assets\",\"audio\":\"assets/audio\"}}};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29uZmlnLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwie1xcXCJCVUlMRFxcXCI6e1xcXCJHQU1FX1RJVExFXFxcIjpcXFwiR0FNRSBUSVRMRVxcXCIsXFxcIlZFUlNJT05cXFwiOlxcXCIwLjAuMVxcXCIsXFxcIldJRFRIXFxcIjo4ODksXFxcIkhFSUdIVFxcXCI6NTAwLFxcXCJNSU5JRklFRF9FWFRFUk5BTF9KU1xcXCI6ZmFsc2UsXFxcIk1JTklGSUVEX0VYVEVSTkFMX0pTX1BBVEhcXFwiOlxcXCIuL3NyYy9leHRlcm5hbC9taW5pZmllZFxcXCIsXFxcIlVOTUlOSUZJRURfRVhURVJOQUxfSlNfUEFUSFxcXCI6XFxcIi4vc3JjL2V4dGVybmFsL3VubWluaWZpZWRcXFwiLFxcXCJFWFRFUk5BTF9KU1xcXCI6W1xcXCJzb3VuZGpzLmpzXFxcIixcXFwicHJlbG9hZGpzLmpzXFxcIl0sXFxcIkFTU0VUUEFUSFxcXCI6e1xcXCJsb2FkXFxcIjpcXFwiYXNzZXRzXFxcIixcXFwiYXVkaW9cXFwiOlxcXCJhc3NldHMvYXVkaW9cXFwifX19XCI/ZDE0YiJdLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHtcIkJVSUxEXCI6e1wiR0FNRV9USVRMRVwiOlwiR0FNRSBUSVRMRVwiLFwiVkVSU0lPTlwiOlwiMC4wLjFcIixcIldJRFRIXCI6ODg5LFwiSEVJR0hUXCI6NTAwLFwiTUlOSUZJRURfRVhURVJOQUxfSlNcIjpmYWxzZSxcIk1JTklGSUVEX0VYVEVSTkFMX0pTX1BBVEhcIjpcIi4vc3JjL2V4dGVybmFsL21pbmlmaWVkXCIsXCJVTk1JTklGSUVEX0VYVEVSTkFMX0pTX1BBVEhcIjpcIi4vc3JjL2V4dGVybmFsL3VubWluaWZpZWRcIixcIkVYVEVSTkFMX0pTXCI6W1wic291bmRqcy5qc1wiLFwicHJlbG9hZGpzLmpzXCJdLFwiQVNTRVRQQVRIXCI6e1wibG9hZFwiOlwiYXNzZXRzXCIsXCJhdWRpb1wiOlwiYXNzZXRzL2F1ZGlvXCJ9fX07Il0sIm1hcHBpbmdzIjoiQUFBQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///Config\n");

/***/ })

/******/ });