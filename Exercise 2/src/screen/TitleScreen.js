import { Container, Text, TEXT_GRADIENT, CanvasRenderer } from "pixi.js";

class TitleScreen extends Container
{
    constructor()
    {
        super();

        var filledCircle = new PIXI.Graphics();
        var filledRectangle = new PIXI.Graphics();
        var filledEllipse = new PIXI.Graphics();
        var outlinedCircle = new PIXI.Graphics();
        var outlinedRectangle = new PIXI.Graphics();
        var outlinedEllipse = new PIXI.Graphics();
        var box = new PIXI.Graphics();
        var sideB = new PIXI.Graphics();
        var sideC = new PIXI.Graphics();
        var sideD = new PIXI.Graphics();
        var star = new PIXI.Graphics(); 

        filledCircle.beginFill(0X98d8d8);
        filledCircle.drawCircle(50, 60, 45);
        filledCircle.endFill();

        filledRectangle.beginFill(0Xc03028);
        filledRectangle.drawRect(100, 100, 175, 210);
        filledRectangle.endFill();

        filledEllipse.beginFill(0X78c850);
        filledEllipse.drawEllipse(200, 400, 180, 75);
        filledEllipse.endFill();

        outlinedCircle.lineStyle(10, 0X98d8d8);
        outlinedCircle.drawCircle(50, 60, 45);

        outlinedRectangle.lineStyle(10, 0Xc03028);
        outlinedRectangle.drawRect(100, 100, 175, 210);

        outlinedEllipse.lineStyle(10, 0X78c850);
        outlinedEllipse.drawEllipse(200, 400, 180, 75);

        box.lineStyle(10, 0XFFFFFF);
        box.moveTo(100, 100);
        box.lineTo(100, 200);
        box.lineTo(150, 200);
        box.lineTo(150, 100);
        box.lineTo(100, 100);

        star.beginFill(0Xf8d030);
        star.drawStar(500, 300, 5, 100, 50, 0);
        star.endFill();

        var IDNumber = new Text("11614162", 
        {
            fill: "White",
            fontSize: 18,
            fontFamily: 'Monofur',
            wordWrap: false,
            align: 'Left'
        });

        var fullName = new Text("Jaynel P. Dugos", 
        {
            fill: [0Xa040a0, 0X78c850, 0X6890f0, 0Xf8d030],
            fontSize: 20,
            fontFamily: 'Monofur',
            wordWrap: false,
            align: 'Left',
            fillGradientType: TEXT_GRADIENT.LINEAR_HORIZONTAL
        });

        var courseName = new Text("Game Development for the Web", 
        {
            fill: [0Xf8d030, 0Xb8b8d0],
            fontSize: 18,
            fontFamily: 'Monofur',
            wordWrap: false,
            align: 'Left',
            fillGradientType: TEXT_GRADIENT.LINEAR_VERTICAL,
            fillGradientStops: [0.5, 0.5]
        });

        var programName = new Text("Bachelor of Science in Interactive Entertaiment and Multimedia Computing major in Game Development", 
        {
            fill: [0X8c850, 0X08030, 0X890f0, 0X8d030],
            fontSize: 18,
            fontFamily: 'Monofur',
            wordWrap: true,
            wordWrapWidth: 150,
            align: 'Left',
            fillGradientType: TEXT_GRADIENT.LINEAR_VERTICAL,
            fillGradientStops: [0.5, 0.5]
        });

        var IGN = new Text("My IGN: Zeineru Hachimitsu", 
        {
            fill: [0Xa8b820, 0X890f0],
            fontSize: 22,
            fontFamily: 'Monofur',
            wordWrap: false,
            align: 'Left',
            fillGradientType: TEXT_GRADIENT.LINEAR_HORIZONTAL,
            fillGradientStops: [0.3, 0.7]
        });

        this.addChild(filledCircle);
        this.addChild(filledRectangle);
        this.addChild(filledEllipse);
        
        setTimeout(()=>
        {
            this.removeChild(filledCircle);
            this.removeChild(filledRectangle);
            this.removeChild(filledEllipse);

            this.addChild(outlinedCircle);
            this.addChild(outlinedRectangle);
            this.addChild(outlinedEllipse);
            
        }, 1500);

        setTimeout(()=>
        {
            this.removeChild(outlinedCircle);
            this.removeChild(outlinedRectangle);
            this.removeChild(outlinedEllipse);

            this.addChild(box);
            this.addChild(sideB);
            this.addChild(sideC);
            this.addChild(sideD);
            this.addChild(star);
        }, 3000);

        setTimeout(()=>
        {
            this.removeChild(box);
            this.removeChild(sideB);
            this.removeChild(sideC);
            this.removeChild(sideD);
            this.removeChild(star);

            this.addChild(IDNumber);
        }, 4500);

        setTimeout(()=>
        {
            this.removeChild(IDNumber);

            this.addChild(fullName);
        }, 6500);

        setTimeout(()=>
        {
            this.removeChild(fullName);

            this.addChild(programName);
        }, 8500);

        setTimeout(()=>
        {
            this.removeChild(programName);

            this.addChild(courseName);
        }, 10500);

        setTimeout(()=>
        {
            this.removeChild(courseName);

            this.addChild(IGN);
        }, 12500);
    }
}

export default TitleScreen;