/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"main": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/index.js","vendor"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/KeyCodes.js":
/*!*************************!*\
  !*** ./src/KeyCodes.js ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\nvar KeyCodes = {\n    KeySpace: 32,\n    KeyArrowLeft: 37,\n    KeyArrowRight: 38,\n    KeyArrowUp: 39,\n    KeyArrowDown: 40,\n    Key0: 48,\n    Key1: 49,\n    Key2: 50,\n    Key3: 51,\n    Key4: 52,\n    Key5: 53,\n    Key6: 54,\n    Key7: 55,\n    Key8: 56,\n    Key9: 57,\n    KeyA: 65,\n    KeyB: 66,\n    KeyC: 67,\n    KeyD: 68,\n    KeyE: 69,\n    KeyF: 70,\n    KeyG: 71,\n    KeyH: 72,\n    KeyI: 73,\n    KeyJ: 74,\n    KeyK: 75,\n    KeyL: 76,\n    KeyM: 77,\n    KeyN: 78,\n    KeyO: 79,\n    KeyP: 80,\n    KeyQ: 81,\n    KeyR: 82,\n    KeyS: 83,\n    KeyT: 84,\n    KeyU: 85,\n    KeyV: 86,\n    KeyW: 87,\n    KeyX: 88,\n    KeyY: 89,\n    KeyZ: 90,\n    KeyNum0: 96,\n    KeyNum1: 97,\n    KeyNum2: 98,\n    KeyNum3: 99,\n    KeyNum4: 100,\n    KeyNum5: 101,\n    KeyNum6: 102,\n    KeyNum7: 103\n};\n\nexports.default = KeyCodes;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvS2V5Q29kZXMuanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL0tleUNvZGVzLmpzPzE3ZDMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIEtleUNvZGVzID0gXHJcbntcclxuICAgIEtleVNwYWNlOiAzMixcclxuICAgIEtleUFycm93TGVmdCA6IDM3LFxyXG4gICAgS2V5QXJyb3dSaWdodCA6IDM4LFxyXG4gICAgS2V5QXJyb3dVcCA6IDM5LFxyXG4gICAgS2V5QXJyb3dEb3duIDogNDAsXHJcbiAgICBLZXkwIDogNDgsXHJcbiAgICBLZXkxIDogNDksXHJcbiAgICBLZXkyIDogNTAsXHJcbiAgICBLZXkzIDogNTEsXHJcbiAgICBLZXk0IDogNTIsXHJcbiAgICBLZXk1IDogNTMsXHJcbiAgICBLZXk2IDogNTQsXHJcbiAgICBLZXk3IDogNTUsXHJcbiAgICBLZXk4IDogNTYsXHJcbiAgICBLZXk5IDogNTcsXHJcbiAgICBLZXlBIDogNjUsXHJcbiAgICBLZXlCIDogNjYsXHJcbiAgICBLZXlDIDogNjcsXHJcbiAgICBLZXlEIDogNjgsXHJcbiAgICBLZXlFIDogNjksXHJcbiAgICBLZXlGIDogNzAsXHJcbiAgICBLZXlHIDogNzEsXHJcbiAgICBLZXlIIDogNzIsXHJcbiAgICBLZXlJIDogNzMsXHJcbiAgICBLZXlKIDogNzQsXHJcbiAgICBLZXlLIDogNzUsXHJcbiAgICBLZXlMIDogNzYsXHJcbiAgICBLZXlNIDogNzcsXHJcbiAgICBLZXlOIDogNzgsXHJcbiAgICBLZXlPIDogNzksXHJcbiAgICBLZXlQIDogODAsXHJcbiAgICBLZXlRIDogODEsXHJcbiAgICBLZXlSIDogODIsXHJcbiAgICBLZXlTIDogODMsXHJcbiAgICBLZXlUIDogODQsXHJcbiAgICBLZXlVIDogODUsXHJcbiAgICBLZXlWIDogODYsXHJcbiAgICBLZXlXIDogODcsXHJcbiAgICBLZXlYIDogODgsXHJcbiAgICBLZXlZIDogODksXHJcbiAgICBLZXlaIDogOTAsXHJcbiAgICBLZXlOdW0wIDogOTYsXHJcbiAgICBLZXlOdW0xIDogOTcsXHJcbiAgICBLZXlOdW0yIDogOTgsXHJcbiAgICBLZXlOdW0zIDogOTksXHJcbiAgICBLZXlOdW00IDogMTAwLFxyXG4gICAgS2V5TnVtNSA6IDEwMSxcclxuICAgIEtleU51bTYgOiAxMDIsXHJcbiAgICBLZXlOdW03IDogMTAzLFxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBLZXlDb2RlczsiXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWpEQTtBQUNBO0FBbURBIiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/KeyCodes.js\n");

/***/ }),

/***/ "./src/KeyValues.js":
/*!**************************!*\
  !*** ./src/KeyValues.js ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\nvar KeyValues = {\n    32: \" \",\n    65: \"A\",\n    66: \"B\",\n    67: \"C\",\n    68: \"D\",\n    69: \"E\",\n    70: \"F\",\n    71: \"G\",\n    72: \"H\",\n    73: \"I\",\n    74: \"J\",\n    75: \"K\",\n    76: \"L\",\n    77: \"M\",\n    78: \"N\",\n    79: \"O\",\n    80: \"P\",\n    81: \"Q\",\n    82: \"R\",\n    83: \"S\",\n    84: \"T\",\n    85: \"U\",\n    86: \"V\",\n    87: \"W\",\n    88: \"X\",\n    89: \"Y\",\n    90: \"Z\"\n\n};\n\nexports.default = KeyValues;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvS2V5VmFsdWVzLmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL3NyYy9LZXlWYWx1ZXMuanM/NjcyOSJdLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgS2V5VmFsdWVzID0gXHJcbntcclxuICAgIDMyIDogXCIgXCIsXHJcbiAgICA2NSA6IFwiQVwiLFxyXG4gICAgNjYgOiBcIkJcIixcclxuICAgIDY3IDogXCJDXCIsXHJcbiAgICA2OCA6IFwiRFwiLFxyXG4gICAgNjkgOiBcIkVcIixcclxuICAgIDcwIDogXCJGXCIsXHJcbiAgICA3MSA6IFwiR1wiLFxyXG4gICAgNzIgOiBcIkhcIixcclxuICAgIDczIDogXCJJXCIsXHJcbiAgICA3NCA6IFwiSlwiLFxyXG4gICAgNzUgOiBcIktcIixcclxuICAgIDc2IDogXCJMXCIsXHJcbiAgICA3NyA6IFwiTVwiLFxyXG4gICAgNzggOiBcIk5cIixcclxuICAgIDc5IDogXCJPXCIsXHJcbiAgICA4MCA6IFwiUFwiLFxyXG4gICAgODEgOiBcIlFcIixcclxuICAgIDgyIDogXCJSXCIsXHJcbiAgICA4MyA6IFwiU1wiLFxyXG4gICAgODQgOiBcIlRcIixcclxuICAgIDg1IDogXCJVXCIsXHJcbiAgICA4NiA6IFwiVlwiLFxyXG4gICAgODcgOiBcIldcIixcclxuICAgIDg4IDogXCJYXCIsXHJcbiAgICA4OSA6IFwiWVwiLFxyXG4gICAgOTAgOiBcIlpcIlxyXG5cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgS2V5VmFsdWVzOyJdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBNUJBO0FBQ0E7QUE4QkEiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/KeyValues.js\n");

/***/ }),

/***/ "./src/Main.js":
/*!*********************!*\
  !*** ./src/Main.js ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\n\nvar _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();\n\nvar _assetdirectory = __webpack_require__(/*! ./assetdirectory */ \"./src/assetdirectory.js\");\n\nvar _pixi = __webpack_require__(/*! pixi.js */ \"./node_modules/pixi.js/lib/index.js\");\n\nvar _TitleScreen = __webpack_require__(/*! ./screen/TitleScreen */ \"./src/screen/TitleScreen.js\");\n\nvar _TitleScreen2 = _interopRequireDefault(_TitleScreen);\n\nvar _ = __webpack_require__(/*! . */ \"./src/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nvar Main = function () {\n    function Main() {\n        _classCallCheck(this, Main);\n    }\n\n    _createClass(Main, null, [{\n        key: \"start\",\n        value: function start() {\n            Main.cjAudioQueue = new createjs.LoadQueue();\n            createjs.Sound.alternateExtensions = [\"ogg\"];\n\n            Main.cjAudioQueue.installPlugin(createjs.Sound);\n            Main.cjAudioQueue.addEventListener(\"complete\", Main.handleAudioComplete.bind(Main));\n\n            if (_assetdirectory.AssetDirectory.audio.length > 0) {\n                //LOAD AUDIO                  \n                var audioFiles = _assetdirectory.AssetDirectory.audio;\n                var audioManifest = [];\n                for (var i = 0; i < audioFiles.length; i++) {\n                    audioManifest.push({\n                        id: audioFiles[i],\n                        src: audioFiles[i]\n                    });\n                }\n                Main.cjAudioQueue.loadManifest(audioManifest);\n            } else {\n                Main.handleAudioComplete();\n            }\n        }\n    }, {\n        key: \"handleAudioComplete\",\n        value: function handleAudioComplete() {\n            if (_assetdirectory.AssetDirectory.load.length > 0) {\n                //LOAD IMAGES         \n                var loader = _pixi.loaders.shared;\n                loader.add(_assetdirectory.AssetDirectory.load);\n                loader.load(Main.handleImageComplete);\n            } else {\n                Main.handleImageComplete();\n            }\n        }\n    }, {\n        key: \"handleImageComplete\",\n        value: function handleImageComplete() {\n            var screen = new _TitleScreen2.default();\n            _.App.stage.addChild(screen);\n        }\n    }]);\n\n    return Main;\n}();\n\nexports.default = Main;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvTWFpbi5qcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9zcmMvTWFpbi5qcz8xMjIxIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEFzc2V0RGlyZWN0b3J5IH0gZnJvbSBcIi4vYXNzZXRkaXJlY3RvcnlcIjtcclxuaW1wb3J0IHsgbG9hZGVycyB9IGZyb20gXCJwaXhpLmpzXCI7XHJcbmltcG9ydCBUaXRsZVNjcmVlbiBmcm9tIFwiLi9zY3JlZW4vVGl0bGVTY3JlZW5cIjtcclxuaW1wb3J0IHsgQXBwIH0gZnJvbSBcIi5cIjtcclxuXHJcbmNsYXNzIE1haW4gXHJcbntcclxuICAgIHN0YXRpYyBzdGFydCgpXHJcbiAgICB7XHJcbiAgICAgICAgTWFpbi5jakF1ZGlvUXVldWUgPSBuZXcgY3JlYXRlanMuTG9hZFF1ZXVlKCk7XHJcbiAgICAgICAgY3JlYXRlanMuU291bmQuYWx0ZXJuYXRlRXh0ZW5zaW9ucyA9IFtcIm9nZ1wiXTtcclxuXHJcbiAgICAgICAgTWFpbi5jakF1ZGlvUXVldWUuaW5zdGFsbFBsdWdpbihjcmVhdGVqcy5Tb3VuZCk7XHJcbiAgICAgICAgTWFpbi5jakF1ZGlvUXVldWUuYWRkRXZlbnRMaXN0ZW5lcihcImNvbXBsZXRlXCIsIE1haW4uaGFuZGxlQXVkaW9Db21wbGV0ZS5iaW5kKE1haW4pKTtcclxuXHJcbiAgICAgICAgaWYoQXNzZXREaXJlY3RvcnkuYXVkaW8ubGVuZ3RoID4gMClcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIC8vTE9BRCBBVURJTyAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBsZXQgYXVkaW9GaWxlcyA9IEFzc2V0RGlyZWN0b3J5LmF1ZGlvO1xyXG4gICAgICAgICAgICBsZXQgYXVkaW9NYW5pZmVzdCA9IFtdO1xyXG4gICAgICAgICAgICBmb3IobGV0IGkgPSAwOyBpIDwgYXVkaW9GaWxlcy5sZW5ndGg7IGkrKylcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgYXVkaW9NYW5pZmVzdC5wdXNoXHJcbiAgICAgICAgICAgICAgICAoXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZDogYXVkaW9GaWxlc1tpXSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc3JjOiBhdWRpb0ZpbGVzW2ldXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBNYWluLmNqQXVkaW9RdWV1ZS5sb2FkTWFuaWZlc3QoYXVkaW9NYW5pZmVzdCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBlbHNlXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBNYWluLmhhbmRsZUF1ZGlvQ29tcGxldGUoKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBoYW5kbGVBdWRpb0NvbXBsZXRlKClcclxuICAgIHtcclxuICAgICAgICBpZihBc3NldERpcmVjdG9yeS5sb2FkLmxlbmd0aCA+IDApXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICAvL0xPQUQgSU1BR0VTICAgICAgICAgXHJcbiAgICAgICAgICAgIGxldCBsb2FkZXIgPSBsb2FkZXJzLnNoYXJlZDtcclxuICAgICAgICAgICAgbG9hZGVyLmFkZChBc3NldERpcmVjdG9yeS5sb2FkKTtcclxuICAgICAgICAgICAgbG9hZGVyLmxvYWQoTWFpbi5oYW5kbGVJbWFnZUNvbXBsZXRlKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSBcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIE1haW4uaGFuZGxlSW1hZ2VDb21wbGV0ZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgaGFuZGxlSW1hZ2VDb21wbGV0ZSgpXHJcbiAgICB7XHJcbiAgICAgICAgbGV0IHNjcmVlbiA9IG5ldyBUaXRsZVNjcmVlbigpO1xyXG4gICAgICAgIEFwcC5zdGFnZS5hZGRDaGlsZChzY3JlZW4pO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBNYWluOyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7OztBQUFBO0FBQ0E7Ozs7O0FBQ0E7Ozs7Ozs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUdBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFDQTtBQUlBO0FBQ0E7QUFFQTs7O0FBR0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFHQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/Main.js\n");

/***/ }),

/***/ "./src/assetdirectory.js":
/*!*******************************!*\
  !*** ./src/assetdirectory.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n\tvalue: true\n});\nvar AssetDirectory = exports.AssetDirectory = {\n\t\"load\": [\"assets/fonts/Monofur.eot\", \"assets/fonts/Monofur.svg\", \"assets/fonts/Monofur.ttf\", \"assets/fonts/Monofur.woff\", \"assets/fonts/Monofur.woff2\", \"assets/fonts/stylesheet.css\", \"assets/images/Comet.png\", \"assets/images/Crescent-Moon.jpg\", \"assets/images/Lightning-Bolt.jpg\", \"assets/images/Lightning.jpg\", \"assets/images/Space-BG.jpg\", \"assets/images/Star.jpg\"],\n\t\"audio\": []\n};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXNzZXRkaXJlY3RvcnkuanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL2Fzc2V0ZGlyZWN0b3J5LmpzPzZmZDEiXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGxldCBBc3NldERpcmVjdG9yeSA9IHtcblx0XCJsb2FkXCI6IFtcblx0XHRcImFzc2V0cy9mb250cy9Nb25vZnVyLmVvdFwiLFxuXHRcdFwiYXNzZXRzL2ZvbnRzL01vbm9mdXIuc3ZnXCIsXG5cdFx0XCJhc3NldHMvZm9udHMvTW9ub2Z1ci50dGZcIixcblx0XHRcImFzc2V0cy9mb250cy9Nb25vZnVyLndvZmZcIixcblx0XHRcImFzc2V0cy9mb250cy9Nb25vZnVyLndvZmYyXCIsXG5cdFx0XCJhc3NldHMvZm9udHMvc3R5bGVzaGVldC5jc3NcIixcblx0XHRcImFzc2V0cy9pbWFnZXMvQ29tZXQucG5nXCIsXG5cdFx0XCJhc3NldHMvaW1hZ2VzL0NyZXNjZW50LU1vb24uanBnXCIsXG5cdFx0XCJhc3NldHMvaW1hZ2VzL0xpZ2h0bmluZy1Cb2x0LmpwZ1wiLFxuXHRcdFwiYXNzZXRzL2ltYWdlcy9MaWdodG5pbmcuanBnXCIsXG5cdFx0XCJhc3NldHMvaW1hZ2VzL1NwYWNlLUJHLmpwZ1wiLFxuXHRcdFwiYXNzZXRzL2ltYWdlcy9TdGFyLmpwZ1wiXG5cdF0sXG5cdFwiYXVkaW9cIjogW11cbn07Il0sIm1hcHBpbmdzIjoiOzs7OztBQUFBO0FBQ0E7QUFjQTtBQWZBIiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/assetdirectory.js\n");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\nexports.App = undefined;\n\nvar _assetdirectory = __webpack_require__(/*! ./assetdirectory.js */ \"./src/assetdirectory.js\");\n\nvar _pixi = __webpack_require__(/*! pixi.js */ \"./node_modules/pixi.js/lib/index.js\");\n\nvar _Main = __webpack_require__(/*! ./Main.js */ \"./src/Main.js\");\n\nvar _Main2 = _interopRequireDefault(_Main);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nvar Config = __webpack_require__(/*! Config */ \"Config\");\n\nvar canvas = document.getElementById('game-canvas');\nvar pixiapp = new _pixi.Application({\n    view: canvas,\n    width: Config.BUILD.WIDTH,\n    height: Config.BUILD.HEIGHT\n});\n\ndocument.body.style.margin = \"0px\";\ndocument.body.style.overflow = \"hidden\";\n\n/*****************************************\r\n ************* ENTRY POINT ***************\r\n *****************************************/\nfunction ready(fn) {\n    if (document.readyState != 'loading') {\n        fn();\n    } else {\n\n        document.addEventListener('DOMContentLoaded', fn);\n    }\n}\n\nready(function () {\n    _Main2.default.start();\n});\n\nexports.App = pixiapp;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvaW5kZXguanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL2luZGV4LmpzPzEyZDUiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQXNzZXREaXJlY3RvcnkgfSBmcm9tICcuL2Fzc2V0ZGlyZWN0b3J5LmpzJztcclxuaW1wb3J0IHsgQXBwbGljYXRpb24gfSBmcm9tICdwaXhpLmpzJztcclxuaW1wb3J0IE1haW4gZnJvbSAnLi9NYWluLmpzJztcclxuXHJcbnZhciBDb25maWcgPSByZXF1aXJlKCdDb25maWcnKTtcclxuXHJcbmxldCBjYW52YXMgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnZ2FtZS1jYW52YXMnKTtcclxubGV0IHBpeGlhcHAgPSBuZXcgQXBwbGljYXRpb25cclxuKFxyXG4gICAge1xyXG4gICAgICAgIHZpZXc6IGNhbnZhcyxcclxuICAgICAgICB3aWR0aDogQ29uZmlnLkJVSUxELldJRFRILFxyXG4gICAgICAgIGhlaWdodDogQ29uZmlnLkJVSUxELkhFSUdIVFxyXG4gICAgfVxyXG4pXHJcblxyXG5kb2N1bWVudC5ib2R5LnN0eWxlLm1hcmdpbiA9IFwiMHB4XCI7XHJcbmRvY3VtZW50LmJvZHkuc3R5bGUub3ZlcmZsb3cgPSBcImhpZGRlblwiO1xyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcbiAqKioqKioqKioqKioqIEVOVFJZIFBPSU5UICoqKioqKioqKioqKioqKlxyXG4gKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbmZ1bmN0aW9uIHJlYWR5KGZuKSBcclxue1xyXG4gICAgaWYgKGRvY3VtZW50LnJlYWR5U3RhdGUgIT0gJ2xvYWRpbmcnKSBcclxuICAgIHtcclxuICAgICAgICBmbigpO1xyXG4gICAgfSBcclxuICAgIFxyXG4gICAgZWxzZSBcclxuICAgIHtcclxuXHJcbiAgICAgICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignRE9NQ29udGVudExvYWRlZCcsIGZuKTtcclxuICAgIH1cclxufVxyXG5cclxucmVhZHkoZnVuY3Rpb24oKSBcclxue1xyXG4gICAgTWFpbi5zdGFydCgpO1xyXG59KTtcclxuXHJcbmV4cG9ydCB7cGl4aWFwcCBhcyBBcHB9OyJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTs7Ozs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUhBO0FBQ0E7QUFNQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/index.js\n");

/***/ }),

/***/ "./src/screen/TitleScreen.js":
/*!***********************************!*\
  !*** ./src/screen/TitleScreen.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\n\nvar _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();\n\nvar _pixi = __webpack_require__(/*! pixi.js */ \"./node_modules/pixi.js/lib/index.js\");\n\nvar _KeyCodes = __webpack_require__(/*! ../KeyCodes */ \"./src/KeyCodes.js\");\n\nvar _KeyCodes2 = _interopRequireDefault(_KeyCodes);\n\nvar _KeyValues = __webpack_require__(/*! ../KeyValues */ \"./src/KeyValues.js\");\n\nvar _KeyValues2 = _interopRequireDefault(_KeyValues);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\"); } return call && (typeof call === \"object\" || typeof call === \"function\") ? call : self; }\n\nfunction _inherits(subClass, superClass) { if (typeof superClass !== \"function\" && superClass !== null) { throw new TypeError(\"Super expression must either be null or a function, not \" + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }\n\nvar TitleScreen = function (_Container) {\n    _inherits(TitleScreen, _Container);\n\n    // Note: Just comment in/out the lines to execute functions \n    //       is weird cause they share same Mouse Event/s.\n\n    function TitleScreen() {\n        _classCallCheck(this, TitleScreen);\n\n        var _this = _possibleConstructorReturn(this, (TitleScreen.__proto__ || Object.getPrototypeOf(TitleScreen)).call(this));\n\n        _this.interactive = true;\n\n        _this.cache = _pixi.utils.TextureCache;\n\n        _this.bolt = new _pixi.Sprite(_this.cache[\"assets/images/Lightning-Bolt.jpg\"]);\n        _this.star = new _pixi.Sprite(_this.cache[\"assets/images/Star.jpg\"]);\n        _this.spaceBG = new _pixi.Sprite(_this.cache[\"assets/images/Space-BG.jpg\"]);\n        _this.comet = new _pixi.Sprite(_this.cache[\"assets/images/Lightning.jpg\"]);\n\n        _this.bolt.x = 50;\n        _this.bolt.y = 50;\n\n        _this.comet.x = 100;\n        _this.comet.y = 100;\n\n        _this.addChild(_this.spaceBG);\n        _this.addChild(_this.bolt);\n        _this.addChild(_this.comet);\n\n        window.addEventListener(\"keydown\", _this.onKeyDown.bind(_this));\n        window.addEventListener(\"keyup\", _this.onKeyUp.bind(_this));\n        window.addEventListener(\"keypress\", _this.onKeyPress.bind(_this));\n        return _this;\n    }\n\n    _createClass(TitleScreen, [{\n        key: \"spawnOnClick\",\n        value: function spawnOnClick(cache, path, container, x, y) {\n            var newSpawn = new _pixi.Sprite(cache[path]);\n\n            newSpawn.x = x;\n            newSpawn.y = y;\n            container.addChild(newSpawn);\n        }\n    }, {\n        key: \"changePositionOnClick\",\n        value: function changePositionOnClick(sprite, x, y) {\n            sprite.x = x;\n            sprite.y = y;\n        }\n    }, {\n        key: \"mousedown\",\n        value: function mousedown(e) {\n            //this.spawnOnClick(this.cache, \"assets/images/Star.jpg\", this, e.data.global.x, e.data.global.y);\n            //this.changePositionOnClick(this.bolt, e.data.global.x, e.data.global.y);\n        }\n    }, {\n        key: \"mousemove\",\n        value: function mousemove(e) {\n            //console.log(\"Mouse Move!: \" + e.data.global.x + \" . \" + e.data.global.y);\n        }\n    }, {\n        key: \"mouseup\",\n        value: function mouseup(e) {\n            //console.log(\"Mouse Up!: \" + e.data.global.x + \" . \" + e.data.global.y);\n        }\n    }, {\n        key: \"onKeyDown\",\n        value: function onKeyDown(e) {\n            //console.log(\"KEYDOWN!: \" + e.keyCode);\n        }\n    }, {\n        key: \"onKeyUp\",\n        value: function onKeyUp(e) {\n            //console.log(\"KEYUP!: \" + e.keyCode);\n        }\n    }, {\n        key: \"onKeyPress\",\n        value: function onKeyPress(e) {\n            //console.log(\"KEYPRESS!: \" + e.keyCode);\n        }\n    }]);\n\n    return TitleScreen;\n}(_pixi.Container);\n\nexports.default = TitleScreen;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvc2NyZWVuL1RpdGxlU2NyZWVuLmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL3NyYy9zY3JlZW4vVGl0bGVTY3JlZW4uanM/YTM3MiJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb250YWluZXIsIHV0aWxzLCBTcHJpdGUsIEdyYXBoaWNzLCB0aWNrZXIgfSBmcm9tIFwicGl4aS5qc1wiO1xyXG5pbXBvcnQgS2V5Q29kZXMgZnJvbSBcIi4uL0tleUNvZGVzXCI7XHJcbmltcG9ydCBLZXlWYWx1ZXMgZnJvbSBcIi4uL0tleVZhbHVlc1wiO1xyXG5cclxuY2xhc3MgVGl0bGVTY3JlZW4gZXh0ZW5kcyBDb250YWluZXJcclxue1xyXG4gICAgLy8gTm90ZTogSnVzdCBjb21tZW50IGluL291dCB0aGUgbGluZXMgdG8gZXhlY3V0ZSBmdW5jdGlvbnMgXHJcbiAgICAvLyAgICAgICBpcyB3ZWlyZCBjYXVzZSB0aGV5IHNoYXJlIHNhbWUgTW91c2UgRXZlbnQvcy5cclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpXHJcbiAgICB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuXHJcbiAgICAgICAgdGhpcy5pbnRlcmFjdGl2ZSA9IHRydWU7XHJcblxyXG4gICAgICAgIHRoaXMuY2FjaGUgPSB1dGlscy5UZXh0dXJlQ2FjaGU7XHJcbiAgICAgICAgXHJcbiAgICAgICAgdGhpcy5ib2x0ID0gbmV3IFNwcml0ZSh0aGlzLmNhY2hlW1wiYXNzZXRzL2ltYWdlcy9MaWdodG5pbmctQm9sdC5qcGdcIl0pO1xyXG4gICAgICAgIHRoaXMuc3RhciA9IG5ldyBTcHJpdGUodGhpcy5jYWNoZVtcImFzc2V0cy9pbWFnZXMvU3Rhci5qcGdcIl0pO1xyXG4gICAgICAgIHRoaXMuc3BhY2VCRyA9IG5ldyBTcHJpdGUodGhpcy5jYWNoZVtcImFzc2V0cy9pbWFnZXMvU3BhY2UtQkcuanBnXCJdKTtcclxuICAgICAgICB0aGlzLmNvbWV0ID0gIG5ldyBTcHJpdGUodGhpcy5jYWNoZVtcImFzc2V0cy9pbWFnZXMvTGlnaHRuaW5nLmpwZ1wiXSk7XHJcblxyXG4gICAgICAgIHRoaXMuYm9sdC54ID0gNTA7XHJcbiAgICAgICAgdGhpcy5ib2x0LnkgPSA1MDtcclxuXHJcbiAgICAgICAgdGhpcy5jb21ldC54ID0gMTAwO1xyXG4gICAgICAgIHRoaXMuY29tZXQueSA9IDEwMDtcclxuICAgICAgICBcclxuICAgICAgICB0aGlzLmFkZENoaWxkKHRoaXMuc3BhY2VCRyk7XHJcbiAgICAgICAgdGhpcy5hZGRDaGlsZCh0aGlzLmJvbHQpO1xyXG4gICAgICAgIHRoaXMuYWRkQ2hpbGQodGhpcy5jb21ldCk7XHJcblxyXG4gICAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwia2V5ZG93blwiLCB0aGlzLm9uS2V5RG93bi5iaW5kKHRoaXMpKTtcclxuICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcImtleXVwXCIsIHRoaXMub25LZXlVcC5iaW5kKHRoaXMpKTtcclxuICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcImtleXByZXNzXCIsIHRoaXMub25LZXlQcmVzcy5iaW5kKHRoaXMpKTtcclxuICAgIH1cclxuXHJcbiAgICBzcGF3bk9uQ2xpY2soY2FjaGUsIHBhdGgsIGNvbnRhaW5lciwgeCwgeSlcclxuICAgIHtcclxuICAgICAgICBsZXQgbmV3U3Bhd24gPSBuZXcgU3ByaXRlKGNhY2hlW3BhdGhdKTtcclxuICAgICAgICBcclxuICAgICAgICBuZXdTcGF3bi54ID0geDtcclxuICAgICAgICBuZXdTcGF3bi55ID0geTtcclxuICAgICAgICBjb250YWluZXIuYWRkQ2hpbGQobmV3U3Bhd24pO1xyXG4gICAgfVxyXG5cclxuICAgIGNoYW5nZVBvc2l0aW9uT25DbGljayhzcHJpdGUsIHgsIHkpXHJcbiAgICB7XHJcbiAgICAgICAgc3ByaXRlLnggPSB4O1xyXG4gICAgICAgIHNwcml0ZS55ID0geTtcclxuICAgIH1cclxuXHJcbiAgICBtb3VzZWRvd24oZSlcclxuICAgIHtcclxuICAgICAgICAvL3RoaXMuc3Bhd25PbkNsaWNrKHRoaXMuY2FjaGUsIFwiYXNzZXRzL2ltYWdlcy9TdGFyLmpwZ1wiLCB0aGlzLCBlLmRhdGEuZ2xvYmFsLngsIGUuZGF0YS5nbG9iYWwueSk7XHJcbiAgICAgICAgLy90aGlzLmNoYW5nZVBvc2l0aW9uT25DbGljayh0aGlzLmJvbHQsIGUuZGF0YS5nbG9iYWwueCwgZS5kYXRhLmdsb2JhbC55KTtcclxuICAgIH1cclxuXHJcbiAgICBtb3VzZW1vdmUoZSlcclxuICAgIHtcclxuICAgICAgICAvL2NvbnNvbGUubG9nKFwiTW91c2UgTW92ZSE6IFwiICsgZS5kYXRhLmdsb2JhbC54ICsgXCIgLiBcIiArIGUuZGF0YS5nbG9iYWwueSk7XHJcbiAgICB9XHJcblxyXG4gICAgbW91c2V1cChlKVxyXG4gICAge1xyXG4gICAgICAgIC8vY29uc29sZS5sb2coXCJNb3VzZSBVcCE6IFwiICsgZS5kYXRhLmdsb2JhbC54ICsgXCIgLiBcIiArIGUuZGF0YS5nbG9iYWwueSk7XHJcbiAgICB9XHJcblxyXG4gICAgb25LZXlEb3duKGUpXHJcbiAgICB7XHJcbiAgICAgICAgLy9jb25zb2xlLmxvZyhcIktFWURPV04hOiBcIiArIGUua2V5Q29kZSk7XHJcbiAgICB9XHJcblxyXG4gICAgb25LZXlVcChlKVxyXG4gICAge1xyXG4gICAgICAgIC8vY29uc29sZS5sb2coXCJLRVlVUCE6IFwiICsgZS5rZXlDb2RlKTtcclxuICAgIH1cclxuXHJcbiAgICBvbktleVByZXNzKGUpXHJcbiAgICB7XHJcbiAgICAgICAgLy9jb25zb2xlLmxvZyhcIktFWVBSRVNTITogXCIgKyBlLmtleUNvZGUpO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBUaXRsZVNjcmVlbjsiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUE7QUFDQTtBQUFBO0FBQ0E7OztBQUFBO0FBQ0E7Ozs7Ozs7Ozs7O0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXhCQTtBQXlCQTtBQUNBOzs7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUVBO0FBQ0E7OztBQUVBO0FBRUE7QUFDQTs7O0FBRUE7QUFFQTtBQUNBOzs7QUFFQTtBQUVBO0FBQ0E7OztBQUVBO0FBRUE7QUFDQTs7OztBQTdFQTtBQUNBO0FBK0VBIiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/screen/TitleScreen.js\n");

/***/ }),

/***/ "Config":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** external "{\"BUILD\":{\"GAME_TITLE\":\"GAME TITLE\",\"VERSION\":\"0.0.1\",\"WIDTH\":889,\"HEIGHT\":500,\"MINIFIED_EXTERNAL_JS\":false,\"MINIFIED_EXTERNAL_JS_PATH\":\"./src/external/minified\",\"UNMINIFIED_EXTERNAL_JS_PATH\":\"./src/external/unminified\",\"EXTERNAL_JS\":[\"soundjs.js\",\"preloadjs.js\"],\"ASSETPATH\":{\"load\":\"assets\",\"audio\":\"assets/audio\"}}}" ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = {\"BUILD\":{\"GAME_TITLE\":\"GAME TITLE\",\"VERSION\":\"0.0.1\",\"WIDTH\":889,\"HEIGHT\":500,\"MINIFIED_EXTERNAL_JS\":false,\"MINIFIED_EXTERNAL_JS_PATH\":\"./src/external/minified\",\"UNMINIFIED_EXTERNAL_JS_PATH\":\"./src/external/unminified\",\"EXTERNAL_JS\":[\"soundjs.js\",\"preloadjs.js\"],\"ASSETPATH\":{\"load\":\"assets\",\"audio\":\"assets/audio\"}}};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29uZmlnLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwie1xcXCJCVUlMRFxcXCI6e1xcXCJHQU1FX1RJVExFXFxcIjpcXFwiR0FNRSBUSVRMRVxcXCIsXFxcIlZFUlNJT05cXFwiOlxcXCIwLjAuMVxcXCIsXFxcIldJRFRIXFxcIjo4ODksXFxcIkhFSUdIVFxcXCI6NTAwLFxcXCJNSU5JRklFRF9FWFRFUk5BTF9KU1xcXCI6ZmFsc2UsXFxcIk1JTklGSUVEX0VYVEVSTkFMX0pTX1BBVEhcXFwiOlxcXCIuL3NyYy9leHRlcm5hbC9taW5pZmllZFxcXCIsXFxcIlVOTUlOSUZJRURfRVhURVJOQUxfSlNfUEFUSFxcXCI6XFxcIi4vc3JjL2V4dGVybmFsL3VubWluaWZpZWRcXFwiLFxcXCJFWFRFUk5BTF9KU1xcXCI6W1xcXCJzb3VuZGpzLmpzXFxcIixcXFwicHJlbG9hZGpzLmpzXFxcIl0sXFxcIkFTU0VUUEFUSFxcXCI6e1xcXCJsb2FkXFxcIjpcXFwiYXNzZXRzXFxcIixcXFwiYXVkaW9cXFwiOlxcXCJhc3NldHMvYXVkaW9cXFwifX19XCI/ZDE0YiJdLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHtcIkJVSUxEXCI6e1wiR0FNRV9USVRMRVwiOlwiR0FNRSBUSVRMRVwiLFwiVkVSU0lPTlwiOlwiMC4wLjFcIixcIldJRFRIXCI6ODg5LFwiSEVJR0hUXCI6NTAwLFwiTUlOSUZJRURfRVhURVJOQUxfSlNcIjpmYWxzZSxcIk1JTklGSUVEX0VYVEVSTkFMX0pTX1BBVEhcIjpcIi4vc3JjL2V4dGVybmFsL21pbmlmaWVkXCIsXCJVTk1JTklGSUVEX0VYVEVSTkFMX0pTX1BBVEhcIjpcIi4vc3JjL2V4dGVybmFsL3VubWluaWZpZWRcIixcIkVYVEVSTkFMX0pTXCI6W1wic291bmRqcy5qc1wiLFwicHJlbG9hZGpzLmpzXCJdLFwiQVNTRVRQQVRIXCI6e1wibG9hZFwiOlwiYXNzZXRzXCIsXCJhdWRpb1wiOlwiYXNzZXRzL2F1ZGlvXCJ9fX07Il0sIm1hcHBpbmdzIjoiQUFBQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///Config\n");

/***/ })

/******/ });