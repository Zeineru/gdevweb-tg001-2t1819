export let AssetDirectory = {
	"load": [
		"assets/fonts/Monofur.eot",
		"assets/fonts/Monofur.svg",
		"assets/fonts/Monofur.ttf",
		"assets/fonts/Monofur.woff",
		"assets/fonts/Monofur.woff2",
		"assets/fonts/stylesheet.css",
		"assets/images/Comet.png",
		"assets/images/Crescent-Moon.jpg",
		"assets/images/Lightning-Bolt.jpg",
		"assets/images/Lightning.jpg",
		"assets/images/Space-BG.jpg",
		"assets/images/Star.jpg"
	],
	"audio": []
};