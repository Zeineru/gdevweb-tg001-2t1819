import { Container, utils, Sprite, Graphics, ticker } from "pixi.js";
import KeyCodes from "../KeyCodes";
import KeyValues from "../KeyValues";

class TitleScreen extends Container
{
    // Note: Just comment in/out the lines to execute functions 
    //       is weird cause they share same Mouse Event/s.

    constructor()
    {
        super();

        this.interactive = true;

        this.cache = utils.TextureCache;
        
        this.bolt = new Sprite(this.cache["assets/images/Lightning-Bolt.jpg"]);
        this.star = new Sprite(this.cache["assets/images/Star.jpg"]);

        this.bolt.x = 50;
        this.bolt.y = 50;
        
        this.addChild(this.bolt);
        this.addChild(this.lightning);
    }

    spawnOnClick(cache, path, container, x, y)
    {
        let newSpawn = new Sprite(cache[path]);
        
        newSpawn.x = x;
        newSpawn.y = y;
        container.addChild(newSpawn);
    }

    changePositionOnClick(sprite, x, y)
    {
        sprite.x = x;
        sprite.y = y;
    }

    mousedown(e)
    {
        //this.spawnOnClick(this.cache, "assets/images/Star.jpg", this, e.data.global.x, e.data.global.y);
        //this.changePositionOnClick(this.bolt, e.data.global.x, e.data.global.y);
    }
}

export default TitleScreen;