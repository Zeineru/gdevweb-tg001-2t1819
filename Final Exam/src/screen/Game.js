import { Container, Graphics, ticker} from "pixi.js";
import KeyCodes from "../KeyCodes";
import KeyValues from "../KeyValues";
import { create } from "domain";
import { ENGINE_METHOD_ALL } from "constants";

class Game extends Container
{
    constructor()
    {
        super();

        this.ticker = ticker.shared;

        this.backGroundMusic = createjs.Sound.play("assets/audio/mp3/Tech_BGM.mp3", "BGM");
        this.backGroundMusic.loop = -1;

        this.playerReference;

        this.bulletList = new Array();
        this.enemyList = new Array();

        this.speed = 20;
        this.timeElapsed = 0;

        this.ticker.add(this.update, this);
    }

    update()
    {
        let deltaTime = this.ticker.elapsedMS / 1000;
        var randomLetter = Math.floor(Math.random() * (91 - 65)) + 65;  
        var randomNumber = Math.floor(Math.random() * 10) * 50;
        var randomTime =  Math.floor(Math.random() * 3) + 1;

        this.timeElapsed += this.ticker.elapsedMS / 1000;
        
        if(this.timeElapsed >= randomTime)
        {
            this.spawnEnemy(randomLetter, randomNumber);
            
            this.timeElapsed = 0;
        }

        for(let i = 0; i < this.bulletList.length; i++)
        {
            //console.log("this.bulletList[i].x: " + this.bulletList[i].x);
            this.bulletList[i].x += this.playerReference.bulletSpeed * deltaTime;
        }

        for(let i = 0; i < this.enemyList.length; i++)
        {
            this.enemyList[i].x -= this.speed * deltaTime;
        }

        for(let i = 0; i < this.bulletList.length; i++)
        {
           for(let j = 0; j < this.enemyList.length; j++)
           {
                if(this.checkCollision(this.bulletList[i], this.enemyList[j]) == true)
                {
                    break;
                }
           }
        }

        this.destroyOnOffBounds(this.bulletList, 0, 900);
        this.destroyOnOffBounds(this.enemyList, 0, 900);

        this.checkGameOver();
    }

    spawnEnemy(randomLetter, randomNumber)
    {
        let enemy = new PIXI.Text(KeyValues[randomLetter],
        {
            fontFamily : 'Monofur', 
            fontSize: 36, 
            fill : 0xFF0000, 
            align : 'center'
        });

        enemy.x = 800;
        enemy.y = randomNumber;

        this.addChild(enemy);
        this.enemyList.push(enemy);
    }

    checkCollision(playerBullet, enemy)
    {
        if((playerBullet.x < (enemy.x + enemy.width)) && 
        (enemy.x < (playerBullet.x + playerBullet.width)) &&
        (playerBullet.y < (enemy.y + enemy.height)) &&
        (enemy.y < (playerBullet.y + playerBullet.height)))
        {
            console.log("Collide!");
            if(playerBullet.text == enemy.text)
            {
                this.bulletList.splice(this.bulletList.indexOf(playerBullet), 1);
                this.enemyList.splice(this.enemyList.indexOf(enemy), 1);
                
                playerBullet.destroy();
                enemy.destroy();
            }
    
            else if(playerBullet.text != enemy.text)
            {
                this.bulletList.splice(this.bulletList.indexOf(playerBullet), 1);
                
                playerBullet.destroy();
            }
    
            return true;
        }
    
        else
        {
           //console.log("Hit");
            return false;
        }
    }

    destroyOnOffBounds(objectList, leftBounds, rightBounds)
    {
        //console.log("Hit");
        for(let i = objectList.length - 1; i >= 0; i--)
        {
            if(objectList[i].x < leftBounds || objectList[i].x > rightBounds)
            {
                //console.log("Hit");
                if(objectList[i].x < leftBounds)
                {
                    this.playerReference.lives--;
                    //console.log(this.playerReference.lives);
                }
                
                //console.log("Remove" + objectList[i]);
                //this.removeChild(objectList[i]);  
                //this.bulletList.splice(this.bulletList.indexOf(playerBullet), 1);
                //objectList[i].destroy();
                objectList.splice(objectList.indexOf(objectList[i]), 1);

                break;
            }
        }
    }

    checkGameOver()
    {
        if(this.playerReference.lives <= 0)
        {
            this.ticker.remove(this.update, this);

            let gameOverText = new PIXI.Text("GAME OVER!",
            {
                fontFamily : 'Monofur', 
                fontSize: 72, 
                fill : 0x00FF00, 
                align : 'center'
            });

            gameOverText.x = 250;
            gameOverText.y = 200;

            this.addChild(gameOverText);
        }
    }
}

export default Game;