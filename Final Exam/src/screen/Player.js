import { Container, Graphics, ticker} from "pixi.js";
import KeyCodes from "../KeyCodes";
import KeyValues from "../KeyValues";
import { create } from "domain";

class Player extends Container
{
    constructor(gameReference)
    {
        super();

        this.gameReference = gameReference;

        this.interactive = true;

        this.ticker = ticker.shared;

        this.player = new PIXI.Text('A',
        {
            fontFamily : 'Monofur', 
            fontSize: 36, 
            fill : 0x0000FF, 
            align : 'center'
        });

        this.lives = 5;

        this.livesCounter = new PIXI.Text("Lives: " + this.lives,
        {
            fontFamily : 'Monofur', 
            fontSize: 28, 
            fill : 0x00FF00, 
            align : 'center'
        });

        this.livesCounter.x = 750;
        this.livesCounter.y = 10;

        this.bulletSpeed = 100;

        this.player.x = 20;
        this.player.y = 50;

        this.addChild(this.player);
        this.addChild(this.livesCounter);

        window.addEventListener("keyup", this.onKeyUp.bind(this));
        this.ticker.add(this.update, this);
    }

    update()
    {
        if(this.lives <= 0)
        {
            this.lives = 0;
        }

        this.livesCounter.text = "Lives: " + this.lives;
    }

    onKeyUp(e)
    {
        console.log("KeyValues: " + KeyValues[e.keyCode]);
        console.log(e.keyCode);

        if((e.keyCode >= 65) && (e.keyCode <= 90))
        {
            let swapSFX = createjs.Sound.play("assets/audio/mp3/Swap.mp3", "SwapSFX");
            this.player.text = "" + KeyValues[e.keyCode];
        }

        if(e.keyCode == KeyCodes.KeySpace)
        {
            let fireSFX = createjs.Sound.play("assets/audio/mp3/Gunfire.mp3", "FireSFX");
            this.spawnBullet();
        }

        this.moveSprite(e);
    }

    moveSprite(e)
    {
        switch(e.keyCode)
        {
            case KeyCodes.KeyArrowUp:
            {
                if(this.player.y <= 0)
                {
                    this.player.y = 0;
                }

                else
                {
                    this.player.y -= 50;
                }
            }

            break;

            case KeyCodes.KeyArrowDown:
            {
                if(this.player.y >= 450)
                {
                    this.player.y = 450;
                }

                else
                {
                    this.player.y += 50;
                }
            }

            break;
        }
    }

    spawnBullet()
    {
        console.log("Shoot");

        let bullet = new PIXI.Text(this.player.text, this.player.style);

        bullet.x = this.player.x;
        bullet.y = this.player.y;

        this.gameReference.addChild(bullet);

        this.gameReference.bulletList.push(bullet);
    }
}

export default Player;