import { Container, Graphics, ticker } from "pixi.js";
import KeyCodes from "../KeyCodes";
import KeyValues from "../KeyValues";
import Game from "../screen/Game"
import Player from "../screen/Player"
import { create } from "domain";

class TitleScreen extends Container
{
    constructor()
    {
        super();

        this.interactive = true;

        this.game = new Game();
        this.player = new Player(this.game);

        this.game.playerReference = this.player;

        this.addChild(this.game);
        this.addChild(this.player.player);
        this.addChild(this.player.livesCounter);
    }
}

export default TitleScreen;