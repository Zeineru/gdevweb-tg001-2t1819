export let AssetDirectory = {
	"load": [
		"assets/fonts/Monofur.eot",
		"assets/fonts/Monofur.svg",
		"assets/fonts/Monofur.ttf",
		"assets/fonts/Monofur.woff",
		"assets/fonts/Monofur.woff2",
		"assets/fonts/stylesheet.css",
		"assets/images/BubbleC.png",
		"assets/images/BubbleG.png",
		"assets/images/BubbleP.png",
		"assets/images/BubbleR.png"
	],
	"audio": [
		"assets/audio/mp3/Gunfire.mp3",
		"assets/audio/mp3/Swap.mp3",
		"assets/audio/mp3/Tech_BGM.mp3"
	]
};