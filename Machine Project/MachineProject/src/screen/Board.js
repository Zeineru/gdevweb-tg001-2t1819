import { Container, Graphics, Sprite, ticker, utils } from "pixi.js";
import KeyCodes from "../KeyCodes";
import KeyValues from "../KeyValues";
import { create } from "domain";
import Tile from "./Tile";

class Board extends Container
{
    constructor()
    {
        super();

        this.interactive = true;
        this.cache = utils.TextureCache;

        this.rows = 11;
        this.columns = 9;

        this.tileWidth = 56;
        this.tileHeight = 56;

        this.boardArray = [];

        this.drawBubbleBoard();
    }

    getBubbleGridLocation(column, row)
    {
        let tileX = column * this.tileWidth;
        let tileY = row * this.tileHeight;

        if((row + 10) % 2)
        {
            tileX = tileX + (this.tileWidth / 2);
        }

        return { tileX: tileX , tileY: tileY };
    }

    drawBubbleBoard()
    {
        for(let i = 0; i < this.rows; i++)
        {
            for(let j = 0; j < this.columns; j++)
            {
                let coordinate = this.getBubbleGridLocation(i, j);

                let bubble = new Tile(coordinate.tileX, coordinate.tileY, this);
            }
        }
    }
}

export default Board;