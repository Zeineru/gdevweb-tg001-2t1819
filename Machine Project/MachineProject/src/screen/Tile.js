import { Container, Graphics, Sprite, ticker, utils } from "pixi.js";
import KeyCodes from "../KeyCodes";
import KeyValues from "../KeyValues";
import { create } from "domain";

class Tile extends Container
{
    constructor(x, y, boardReference)
    {
        super();

        this.interactive = true;
        this.cache = utils.TextureCache;

        this.x = x;
        this.y = y;

        this.boardReference = boardReference;

        this.tileWidth = 56;
        this.tileHeight = 56;

        for(let i = 0; i < 5; i++)
        {
            this.tileType = Math.floor(Math.random() * 4);
        }

        this.redBubble = new Sprite(this.cache["assets/images/BubbleR.png"]);
        this.greenBubble = new Sprite(this.cache["assets/images/BubbleG.png"]);
        this.cyanBubble = new Sprite(this.cache["assets/images/BubbleC.png"]);
        this.pinkBubble = new Sprite(this.cache["assets/images/BubbleP.png"]);

        this.bubbleList = new Array();

        this.bubbleList.push(this.redBubble);
        this.bubbleList.push(this.greenBubble);
        this.bubbleList.push(this.cyanBubble);
        this.bubbleList.push(this.pinkBubble);

        this.drawBubble(boardReference);
    }

    drawBubble(container)
    {
        let bubble = this.bubbleList[this.tileType];

        bubble.x = this.x + 220;
        bubble.y = this.y;
        bubble.width = 56;
        bubble.height = 56;

        container.addChild(bubble);
    }
}

export default Tile;