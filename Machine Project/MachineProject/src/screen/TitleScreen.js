import { Container, Graphics, Sprite, ticker, utils } from "pixi.js";
import KeyCodes from "../KeyCodes";
import KeyValues from "../KeyValues";
import { create } from "domain";
import Tile from "./Tile";
import Board from "./Board";

class TitleScreen extends Container
{
    constructor()
    {
        super();

        this.interactive = true;

        this.board = new Board();

        this.addChild(this.board);
    }
}

export default TitleScreen;