export let AssetDirectory = {
	"load": [
		"assets/fonts/Monofur.eot",
		"assets/fonts/Monofur.svg",
		"assets/fonts/Monofur.ttf",
		"assets/fonts/Monofur.woff",
		"assets/fonts/Monofur.woff2",
		"assets/fonts/stylesheet.css"
	],
	"audio": []
};