import { Container, utils, Sprite, Graphics } from "pixi.js";
import Button from "../objects/Button";

class Board
{   
    constructor(x, y, backgroundColor, backgroundWidth, backgroundHeight, container)
    {
        this.x = x;
        this.y = y;
        this.backgroundColor = backgroundColor;
        this.backgroundWidth = backgroundWidth;
        this.backgroundHeight = backgroundHeight;
        this.container = container;

        this.graphics = new Graphics;

        this.graphics.beginFill(backgroundColor);
        this.graphics.drawRect(x, y, backgroundWidth, backgroundHeight);
        this.graphics.endFill();

        this.container.addChild(this.graphics);

        this.drawBoxes(this.container);
        //this.drawNotches(this.container);
        //this.drawIndicators(this.container);
    }

    drawBoxes(container)
    {
        var box1 = new Graphics;
        var box2 = new Graphics;

        for(let i = 0; i < 12; i++)
        {
            let biggerBox = new Graphics;
            let smallerBox = new Graphics;
            let playerAnswerBox = new Graphics;
            let correctAnswerBox = new Graphics;

            biggerBox.lineStyle(4, 0X78c850);
            biggerBox.drawRect(10 + i * 125, 10, 125, 250);

            smallerBox.lineStyle(4, 0X78c850);
            smallerBox.drawRect(10 + i * 125, 300, 125, 125);

            playerAnswerBox.lineStyle(3, 0X78c850);
            playerAnswerBox.drawRect(525, 480, 125, 250);

            correctAnswerBox.lineStyle(4, 0X78c850);
            correctAnswerBox.drawRect(650, 500, 275, 125);

            container.addChild(biggerBox);
            container.addChild(smallerBox);
            container.addChild(playerAnswerBox);
            //container.addChild(correctAnswerBox)
        }
    }
}

export default Board