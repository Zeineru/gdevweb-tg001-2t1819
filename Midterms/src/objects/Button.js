import { Container, utils, Sprite, Graphics } from "pixi.js";

class Button
{
    constructor(buttonName, buttonID, x, y, radius, color, container, playerChoices)
    {
        this.buttonName = "" + buttonName;
        this.buttonID = buttonID;
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.color = color;
        this.container = container;

        this.playerChoices = playerChoices;
        this.playerChoices = new Array();

        var playerChoiceButtons = new Array();

        var originalX = this.x;
        var originalY = this.y;

        this.graphics = new PIXI.Graphics();

        this.graphics.beginFill(color);
        this.graphics.drawCircle(x, y, radius);
        this.graphics.endFill();
        this.graphics.interactive = true;

        this.graphics.click = this.onButtonDown.bind(this);

        this.container.addChild(this.graphics);
    }

    onButtonDown()
    {
        //this.playerChoices = this.addToList(this.playerChoices);

        this.playerChoices = this.selectButton();
        this.playerChoices = this.unselectButton();
    }

    getButtonID()
    {
        return this.buttonID;
    }

    addToList(playerChoices)
    {
        this.playerChoices = playerChoices;

        if(playerChoices.length < 4)
        {
            let a = new Button(this.buttonName, this.buttonID, 590, 540 + 55 * playerChoices.length, this.radius, this.color, this.container, this.playerChoices);

            a.listOrder = playerChoices.length;

            playerChoices.push(a);

            //return playerChoices;
        }

        else
        {
            return playerChoices;
        }
    }

    selectButton()
    {
        if(this.playerChoices.length < 4 || this.playerChoiceButtons.length < 4)
        {       
            var x = this.getButtonID();

            this.playerChoices = new Array();
            this.playerChoiceButtons = new Array();

            this.playerChoices.splice(x);
            this.playerChoiceButtons.splice(this);

            this.moveButtonToChoices(this);

            this.click = this.unselectButton.bind(this);

            return this.playerChoices;
        }

        else
        {
            return this.playerChoices;
        }
    }

    unselectButton()
    {
        if(this.playerChoices.length == 4 || this.playerChoiceButtons.length == 4)
        {
            var x = this.getButtonID();

            this.playerChoices = new Array();
            this.playerChoiceButtons = new Array();
    
            this.playerChoices.push(this);
            this.playerChoiceButtons.push(x);
    
            this.removeButtonToChoices(this);
    
            this.click = this.selectButton.bind(this);
    
            return this.playerChoices;
        }

        else
        {
            return this.playerChoices; 
        }
    }

    moveButtonToChoices(button)
    {
        this.button = button;

        button.graphics.x = 50;
        //button.graphics.y = 20;
    }

    removeButtonToChoices(button)
    {
        this.button = button;
        var originalX = this.originalX;

        button.graphics.x = originalX;
        //button.graphics.y = 20;
    }
}

export default Button;