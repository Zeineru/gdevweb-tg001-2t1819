import { Container, utils, Sprite, Graphics } from "pixi.js";

class Game
{   
    constructor(maxRounds, choicesList)
    {
        this.rounds = maxRounds;
        this.choicesList = choicesList;

        var correctList = new Array();
        var playerAnswers = new Array();
        var counterList =  new Array();

        var round = 0;
    }
    
    checkAnswers(playerAnswers, correctAnswers, countersList)
    {
        this.playerAnswers = playerAnswers;
        this.correctAnswers = correctAnswers;
        this.countersList = countersList;

        countersList = this.clearList(countersList);

        var whiteCounter = 0;
        var blackCounter = 0;
        var wrongCounter = 0;

        for(let i = 0; i < playerAnswers.length; i++) 
        {
            //console.log(playerAnswers[i] + " == " + correctAnswers[i]);
            //console.log("playerAnswers.includes " + correctAnswers[i] + " && " + playerAnswers[i] + " != " + correctAnswers[i]);
            //console.log("!playerAnswers.includes " + correctAnswers[i]);

            if(playerAnswers[i] == correctAnswers[i])
            {
                whiteCounter++;
            }
        
            else if(playerAnswers.includes(correctAnswers[i]) && (playerAnswers[i] != correctAnswers[i]))
            {
                blackCounter++;
            }
        
            else if(!playerAnswers.includes(correctAnswers[i]))
            {
                wrongCounter++;
            }
        }

        countersList.push(whiteCounter);
        countersList.push(blackCounter);
        countersList.push(wrongCounter);

        return countersList;
    }

    generateCorrectAnswer(choicesList)
    {
        var tempList = new Array();

        while(tempList.length < 4)
        {
            let x = choicesList.length;

            let random =  Math.floor(Math.random() * Math.floor(x));
            
            if(!tempList.includes(choicesList[random]))
            {
                tempList.push(choicesList[random]);
            }
        }

        for(let i = 0; i < 4; i++)
        {
            console.log(tempList[i]);
        }

        return tempList;
    }

    clearList(list)
    {
        this.list = list;
        list = new Array();

        for(let i = 0; i < list.length; i++)
        {
            list.pop();
        }

        return list;
    }

    hasWon()
    {
        if(counterList[0] == 4)
        {
            return true;
        }

        else
        {
            return false;
        }
    }

    endGame()
    {
        if((this.rounds >= this.maxRounds) && hasWon() == true)
        {
            // Display You Won!
            // Display Correct Answer
        }

        else if((this.rounds >= this.maxRounds) && hasWon() == false)
        {
            // Display You Lose!
            // Display Correct Answer
        }
    }
}

export default Game;