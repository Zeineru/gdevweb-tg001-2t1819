import { Container, utils, Sprite, Graphics, Text } from "pixi.js";

class UIButton
{
    constructor(buttonType, x, y, height, width, color, container, buttonText, gameReference)
    {
        this.buttonType = buttonType;
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
        this.color = color;
        this.container = container;

        this.buttonText = "" + buttonText;

        this.gameReference = gameReference;

        var isClicked = false;

        this.graphics = new Graphics;

        this.graphics.click = this.onButtonDown.bind(this);

        this.graphics.lineStyle(3, color);
        this.graphics.drawRect(x, y, width, height);

        this.graphics.interactive = true;

        var buttonText = new Text(this.buttonText,
        {
            fill: "#f8d030",
            fontSize: 28,
            fontFamily: 'Monofur',
            wordWrap: false,
            align: 'Right',
        });

        buttonText.x = this.x + 25;
        buttonText.y = this.y + 75;

        this.container.addChild(this.graphics); 
        this.container.addChild(buttonText);   
    }

    onButtonDown()
    {
        this.isClicked = true;

        switch(this.buttonType)
        {
            case 1:
            {
                console.log("Hit");
                submitAnswers();
            }

            break;

            case 2:
            {

            }

            break;
        }
    }

    submitAnswers()
    {
        var results = new Array();
        
        results = gameReference.checkAnswers(gameReference.playerAnswers, gameReference.correctList);

        gameReference.round++;
        //gameReference.pushResults(compareArr);

        gameReference.clearList(gameReference.playerAnswers);

        if(gameReference.round == gameReference.maxRounds || gameReference.isWon == true)
        {
            gameReference.endGame();
        }

        return results;
    }
}

export default UIButton;