import { Container, utils, Sprite, Graphics, Text } from "pixi.js";
import Button from "../objects/Button";
import Board from "../objects/Board";
import Game from "../objects/Game";
import UIButton from "../objects/UIButton";

var config = require('Config');

class TitleScreen extends Container
{   
    constructor()
    {
        super();

        var game = new Game(12, arrayList);

        var board = new Board(0, 0, 0X000000, config.BUILD.WIDTH, config.BUILD.HEIGHT, this);

        var arrayList = new Array(1, 2, 3, 4, 5, 6, 7, 8);

        var playerAnswersList = new Array();

        var correctAnswerList = new Array();

        var counterList = new Array();

        var colorsList = new Array();

        var counterList = new Array();
        
        var submitButton = new UIButton(1, 700, 500, 150, 150, 0X78c850, this, "Submit", game);

        var correctAnswerList = game.generateCorrectAnswer(arrayList);

        this.createColorButtons(this, this.colorsList);

        if(submitButton.isClicked == true)
        {
            console.log("Hit");
            this.counterList = game.checkAnswers(playerAnswersList, correctAnswerList, counterList);

            for(let i = 0; i < this.counterList.length; i++)
            {
                console.log(counterList[i]);
            }
        }

        var testChoices = new Array(1, 3, 7, 5);

        this.counterList = game.checkAnswers(testChoices, correctAnswerList, counterList);

        for(let i = 0; i < this.counterList.length; i++)
        {
            console.log(this.counterList[i]);
        }

        //this.functionTest(this.x, this.y);

        /*var x = 55;

        var circle = new Graphics;
        circle.lineStyle(5, 0XFFFFFF);
        circle.drawCircle(70, x * 1, 25);
        this.addChild(circle);

        var circle2 = new Graphics;
        circle2.lineStyle(5, 0XFFFFFF);
        circle2.drawCircle(70, x * 2, 25);
        this.addChild(circle2);

        var circle3 = new Graphics;
        circle3.lineStyle(5, 0XFFFFFF);
        circle3.drawCircle(70, x * 3, 25);
        this.addChild(circle3);

        var circle4 = new Graphics;
        circle4.lineStyle(5, 0XFFFFFF);
        circle4.drawCircle(70, x * 4, 25);
        this.addChild(circle4);*/

        //var circle5 = new Graphics;
        //circle5.lineStyle(5, 0XFFFFFF);
        //circle5.drawCircle(590, 530, 25);
        //this.addChild(circle5);
    }

    createColorButtons(container, colorsList)
    {
        colorsList = new Array();

        var red = 0Xc03028;
        var orange = 0Xf08030;
        var yellow = 0Xf8d030;
        var green = 0Xa8b820;
        var blue = 0X6890f0;
        var purple = 0Xa890f0;
        var pink = 0Xf85888;
        var brown = 0Xb8a038;
        var black = 0X000000;
        var white = 0XFFFFFF;

        colorsList[0] = red;
        colorsList[1] = orange;
        colorsList[2] = yellow;
        colorsList[3] = green;
        colorsList[4] = blue;
        colorsList[5] = purple;
        colorsList[6] = pink;
        colorsList[7] = brown;

        var redButton = new Button("Red", 1, 70, 550, 25, colorsList[0], container, this.playerAnswersList);
        var orangeButton = new Button("Orange", 2, 200, 550, 25, colorsList[1], container, this.playerAnswersList);
        var yellowButton = new Button("Yellow", 3, 330, 550, 25, colorsList[2], container, this.playerAnswersList);
        var greenButton = new Button("Green", 4, 460, 550, 25, colorsList[3], container, this.playerAnswersList);

        var blueButton = new Button("Blue", 5,  70, 650, 25, colorsList[4], container, this.playerAnswersList);
        var purpleButton = new Button("Purple", 6, 200, 650, 25, colorsList[5], container, this.playerAnswersList);
        var pinkButton = new Button("Pink", 7, 330, 650, 25, colorsList[6], container, this.playerAnswersList);
        var brownButton = new Button("Brown", 8, 460, 650, 25, colorsList[7], container, this.playerAnswersList);
    }

    gameFlow()
    {
        
    }
}

export default TitleScreen;