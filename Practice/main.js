const app = new PIXI.Application
(	
	{
		"width": 1360,
		"height": 768,
		"view": document.getElementById("game_canvas"),
		"backgroundColor": 0x000000,
		"autoStart": true
	}
);

var graphics = new PIXI.Graphics();

graphics.lineStyle(3, 0Xc03028);
graphics.drawCircle(200, 300, 50);

graphics.lineStyle(4, 0X78C850);
graphics.drawRect(100, 500, 400, 50);

graphics.lineStyle(2, 0X6890f0);
graphics.moveTo(550, 500);
graphics.lineTo(10, 15);

graphics.beginFill(0Xf8d030);
graphics.lineStyle(0, 0xFFFFFF);
graphics.drawCircle(650, 450, 50);
graphics.endFill();

graphics.beginFill(0xf08030);
graphics.lineStyle(0, 0xFFFFFF);
graphics.drawRect(5, 600, 650, 50);
graphics.endFill();

graphics.beginFill(0Xf6890f0);
graphics.lineStyle(1, 0Xb8a038);
graphics.drawCircle(800, 500, 75);
graphics.endFill();

graphics.beginFill(0Xc03028);
graphics.lineStyle(3, 0Xa040a0);
graphics.drawRect(200, 100, 250, 40);
graphics.endFill();

graphics.beginFill(0Xf6890f0);
graphics.lineStyle(1, 0Xb8a038);
graphics.drawEllipse(550, 200, 100, 50);
graphics.endFill();

var pentagonPoints = [];
var sides = 5;
var radius = 75;
var x = 700;
var y = 100;

for(let i = 0; i <= 5; i++) 
{
	pentagonPoints.push(new PIXI.Point(x + radius * Math.cos(2 * Math.PI * i / sides),
							y + radius * Math.sin(2 * Math.PI * i / sides)));
}

graphics.lineStyle(4, 0Xf85888);
graphics.drawPolygon(pentagonPoints);

// Draw The Stickman

graphics.beginFill(0X000000);
graphics.lineStyle(15, 0XFFFFFF);
graphics.drawCircle(1220, 150, 65);
graphics.endFill();

/*graphics.lineStyle(15, 0XFFFFFF);
graphics.moveTo(1250, 200);
graphics.lineTo(350, 350);*/

app.stage.addChild(graphics);