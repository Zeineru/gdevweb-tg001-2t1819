//console.log("Hello World!");
//console.error("THIS IS AN ERROR!!!")
//console.warn("WARNING!!!")

var name = "DUGOS, Jaynel P.";
var ID = 11614162;

var box = 
{ 
	"name" : "Box",
	"ID" : "1"
};

console.log(box.name);
console.log(box["name"]);

function addNumbers(paramA, paramB)
{
	var sum = paramA + paramB;

	return sum;
};

console.log(Number.POSITIVE_INFINITY);
console.log(addNumbers(1, Number.POSITIVE_INFINITY));

var numberOne = "1";
var numberOnePointFive = "1.5";

parseInt(numberOne);
parseFloat(numberOnePointFive);

console.log(numberOne);
console.log(numberOnePointFive);