export let AssetDirectory = {
	"load": [
		"assets/fonts/Monofur.eot",
		"assets/fonts/Monofur.svg",
		"assets/fonts/Monofur.ttf",
		"assets/fonts/Monofur.woff",
		"assets/fonts/Monofur.woff2",
		"assets/fonts/stylesheet.css",
		"assets/images/Lightning.jpg"
	],
	"audio": [
		"assets/audio/mp3/02 - Beethoven：Violin Sonata No. 9 Kreutzer First Movement.mp3",
		"assets/audio/mp3/11 - Chopin-Ysaye Ballade No. 1.mp3",
		"assets/audio/mp3/Joker_PersonaCall.mp3",
		"assets/audio/mp3/Makoto_PersonaCall.mp3",
		"assets/audio/mp3/Persona 5 - Last Surprise OFFICIAL Lyrics.mp3"
	]
};