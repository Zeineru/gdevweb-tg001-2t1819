import { Container } from "pixi.js";
import KeyCodes from "../KeyCodes";
import { create } from "domain";

class TitleScreen extends Container
{
    constructor()
    {

        super();

        this.battleMusic = createjs.Sound.play("assets/audio/mp3/Persona 5 - Last Surprise OFFICIAL Lyrics.mp3", "BattleMusic");

        this.protagonistSFX = createjs.Sound.play("assets/audio/mp3/Joker_PersonaCall.mp3", "ProtagonistSFX");

        this.allySFX = createjs.Sound.play("assets/audio/mp3/Makoto_PersonaCall.mp3", "AllySFX");
        
        window.addEventListener("keyup", this.onKeyUp.bind(this));
    }

    onKeyUp(e)
    {
        switch(e.keyCode)
        {
            case KeyCodes.KeyArrowUp:
            {
               if(this.battleMusic.volume >= 1.0)
               {
                 this.battleMusic.volume = 1.0;
               }

               else
               {
                  this.battleMusic.volume += 0.1;
               }

               if(this.protagonistSFX.volume >= 1.0)
               {
                 this.protagonistSFX.volume = 1.0;
               }

               else
               {
                  this.protagonistSFX.volume += 0.1;
               }

               if(this.allySFX.volume >= 1.0)
               {
                 this.allySFX.volume = 1.0;
               }

               else
               {
                  this.allySFX.volume += 0.1;
               }

               break;
            }

            case KeyCodes.KeyArrowDown:
            {
                if(this.battleMusic.volume <= 0.1)
                {
                  this.battleMusic.volume = 0.1;
                }
 
                else
                {
                   this.battleMusic.volume -= 0.1;
                }
 
                if(this.protagonistSFX.volume <= 0.1)
                {
                  this.protagonistSFX.volume = 0.1;
                }
 
                else
                {
                   this.protagonistSFX.volume -= 0.1;
                }
 
                if(this.allySFX.volume <= 0.1)
                {
                  this.allySFX.volume = 0.1;
                }
 
                else
                {
                   this.allySFX.volume -= 0.1;
                }

                break;
            }

            case KeyCodes.KeyBackSpace:
            {
                if(this.battleMusic.playState == createjs.Sound.PLAY_SUCCEEDED)
                {
                    this.battleMusic.muted = !this.battleMusic.muted;
                }
        
                else
                {
                    this.battleMusic.play();
                }

                if(this.protagonistSFX.playState == createjs.Sound.PLAY_SUCCEEDED)
                {
                    this.musicInstance.muted = !this.musicInstance.muted;
                }
        
                else
                {
                    this.musicInstance.play();
                }

                if(this.allySFX.playState == createjs.Sound.PLAY_SUCCEEDED)
                {
                    this.musicInstance.muted = !this.musicInstance.muted;
                }
        
                else
                {
                    this.musicInstance.play();
                }

                break;

            }

            case KeyCodes.Key1:
            {
                this.protagonistSFX.play();

                break;
            }

            case KeyCodes.Key2:
            {
                this.allySFX.play();

                break;
            }

            case KeyCodes.KeyQ:
            {
                if(this.protagonistSFX.volume >= 1.0)
                {
                  this.protagonistSFX.volume = 1.0;
                }
 
                else
                {
                   this.protagonistSFX.volume += 0.1;
                }

                break;
            }

            case KeyCodes.KeyW:
            {
                if(this.allySFX.volume >= 1.0)
                {
                  this.allySFX.volume = 1.0;
                }
 
                else
                {
                   this.allySFX.volume += 0.1;
                }

                break;
            }

            case KeyCodes.KeyA:
            {
                if(this.protagonistSFX.volume <= 0.1)
                {
                  this.protagonistSFX.volume = 0.1;
                }
 
                else
                {
                   this.protagonistSFX.volume -= 0.1;
                }

                break;
            }

            case KeyCodes.KeyS:
            {
                if(this.allySFX.volume <= 0.1)
                {
                  this.allySFX.volume = 0.1;
                }
 
                else
                {
                   this.allySFX.volume -= 0.1;
                }

                break;
            }

            case KeyCodes.KeyZ:
            {
                if(this.protagonistSFX.playState == createjs.Sound.PLAY_SUCCEEDED)
                {
                    this.protagonistSFX.muted = !this.protagonistSFX.muted;
                }
        
                else
                {
                    this.protagonistSFX.play();
                }

                break;
            }

            case KeyCodes.KeyX:
            {
                if(this.allySFX.playState == createjs.Sound.PLAY_SUCCEEDED)
                {
                    this.allySFX.muted = !this.allySFX.muted;
                }
        
                else
                {
                    this.allySFX.play();
                }

                break;
            }

            case KeyCodes.Key3:
            {
                if(this.battleMusic.playState == createjs.Sound.PLAY_SUCCEEDED)
                {
                    this.battleMusic.stop();
                }
        
                else
                {
                    this.battleMusic.play();
                }

                break;
            }

            case KeyCodes.KeyE:
            {
                if(this.battleMusic.volume >= 1.0)
                {
                  this.battleMusic.volume = 1.0;
                }
 
                else
                {
                   this.battleMusic.volume += 0.1;
                }

                break;
            }

            case KeyCodes.KeyD:
            {
                if(this.battleMusic.volume <= 0.1)
                {
                  this.battleMusic.volume = 0.1;
                }
 
                else
                {
                   this.battleMusic.volume -= 0.1;
                }

                break;
            }

            case KeyCodes.KeyF:
            {
                if(this.battleMusic.playState == createjs.Sound.PLAY_SUCCEEDED)
                {
                    this.battleMusic.muted = !this.battleMusic.muted;
                }
        
                else
                {
                    this.battleMusic.play();
                }

                break;
            }

            case KeyCodes.KeyR:
            {
                this.battleMusic.paused = !this.battleMusic.paused;

                break;
            }
        }
    }
}

export default TitleScreen;