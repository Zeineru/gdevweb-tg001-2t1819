class Point
{
	constructor(x, y)
	{
		this.x = x;
		this.y = y;
	}

	getLength()
	{
		return Math.sqrt(this.x * this.x + this.y * this.y);
	}

	static addNumbers(point)
	{
		this.x += point.x;
		this.y += point.y;

		return this;
	}
}