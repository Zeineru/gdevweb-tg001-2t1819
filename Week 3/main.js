const app = new PIXI.Application
(	
	{
		"width": 1360,
		"height": 768,
		"view": document.getElementById("game_canvas"),
		"backgroundColor": 0X705848,
		"autoStart": true
	}
);

var graphics = new PIXI.Graphics();

graphics.lineStyle(3, 0xFF0000);
graphics.drawCircle(200, 300, 50);

graphics.lineStyle(4, 0X78C850);
graphics.drawRect(100, 500, 400, 50);

graphics.lineStyle(2, 0X6890f0);
graphics.moveTo(550, 500);
graphics.lineTo(10, 15);

graphics.beginFill(0Xf8d030);
graphics.lineStyle(10, 0xFFFFFF);
graphics.drawCircle(650, 450, 50);
graphics.endFill();

//graphics.beginFill(0Xf8d030);
//graphics.lineStyle(10, 0xFFFFFF);
graphics.drawRect(150, 640, 620, 250);
//graphics.endFill();

app.stage.addChild(graphics);