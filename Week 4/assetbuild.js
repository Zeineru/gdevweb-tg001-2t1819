/*var assetPath = {
    load: 'assets',
    preload: 'assets/preload',
}*/

var fs = require('fs');
var path = require('path');
var config = JSON.parse( fs.readFileSync('./config.json', 'utf8') ).BUILD;
var assetPath = config.ASSETPATH || { load: 'assets', audio: 'assets/audio' };


module.exports.generateAssetList = ()=> {
    console.log("Building Assets");
    var assetDirectory = Object.create(null);
    var list = Object.create(null);
    var apkey = Object.keys(assetPath);
    for (var i = 0; i < apkey.length; i++) {
        list[apkey[i]] = [];
    }
    var stream = fs.createWriteStream("src/assetdirectory.js");

    function getPath(dir) {
        var items = fs.readdirSync(dir);
        for (var i = 0; i < items.length; i++) {
            var file = dir + '/' + items[i];

            var stat = fs.statSync(file);
            if (stat.isFile()) {
                for (var j = apkey.length; j >= 0; j--) {
                    if (file.includes(assetPath[apkey[j]]) && !file.endsWith('.ogg')) {
                        list[apkey[j]].push(file);
                        break;
                    }
                }
            }
            if (stat.isDirectory()) {
                getPath(file);
            }
        }
    }

    function generateList() {
        for (var i = 0; i < apkey.length; i++) {
            assetDirectory[apkey[i]] = list[apkey[i]];
        }
        var jsonObj = JSON.stringify(assetDirectory, null, '\t');
        stream.once('open', function (fd) {
            stream.write("export let AssetDirectory = ");
            stream.write(jsonObj);
            stream.write(";");
            stream.end()
        })
    }

    getPath(assetPath.load);
    generateList();
    console.log("Done  Assets");
    return stream;
}

// generateAssetList();