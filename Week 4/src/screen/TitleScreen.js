import { Container, Text, TEXT_GRADIENT } from "pixi.js";

class TitleScreen extends Container
{
    constructor()
    {
        super();

        var sampleText = new Text("\nおまえ　わ　も　しんでいる！", 
        {
            // fill: 'Fire Orange, Psychic Pink, Water Blue, Electric Yellow
            fill: ["#f08030", "#f85888", "#6890f0", "#f8d030"],
            fontSize: 28,
            fontFamily: 'Monofur',
            wordWrap: false,
            //wordWrapWidth: 150,
            align: 'Right',
            fillGradientType: TEXT_GRADIENT.LINEAR_HORIZONTAL,
            fillGradientStops: [0.8, 0.2]
        });

        this.addChild(sampleText);

        setTimeout(()=>
        {
            sampleText.text = "\nなに！"
        }, 1000);
    }
}

export default TitleScreen;