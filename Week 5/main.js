const app = new PIXI.Application
(	
	{
		"width": 1360,
		"height": 768,
		"view": document.getElementById("game_canvas"),
		"backgroundColor": 0X00000,
		"autoStart": true
	}
);

var loader = PIXI.loaders.shared;

var imageList = [{name: "Golem", url: "images/Alolan-Golem.png"}, 
				{name: "raichu", url: "images/Alolan-Raichu.png"}, 
				{name: "Jolteon", url: "images/Jolteon.png"}, 
				{name: "Lanturn", url: "images/Lanturn.png"}, 
				{name: "Luxray", url: "images/Luxray.png"}, 
				{name: "Magnezone", url: "images/Magnezone.png"}];

loader.add(imageList);

var onLoadComplete = ()=>
{
	var textureCache = PIXI.utils.TextureCache;
	var spritesList = [];

	for(let i = 0; i < imageList.length; i++)
	{
		spritesList.push(new PIXI.Sprite(textureCache[imageList[i]["name"]]));
	}

	for(let i = 0; i < spritesList.length; i++)
	{
		app.stage.addChild(spritesList[i]);
	}
}

var onLoadProgress = (resource)=>
{
	console.log("There is progress.")
	console.log(loader.progress);
}

loader.onProgress.add(onLoadProgress);

var onLoadAsset = (loader, resource)=>
{
	console.log("Loaded: ");
	console.log(resource.url);
}

loader.onLoad.add(onLoadAsset);

var onLoadError = (error, loader, resource)=>
{
	console.log("Load Error!");
	console.log(resource);
}

loader.onError.add(onLoadError);

loader.load(onLoadComplete);