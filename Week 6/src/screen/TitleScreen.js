import { Container, utils, Sprite, Graphics } from "pixi.js";

class TitleScreen extends Container
{
    constructor()
    {
        super();

        this.interactive = true;

        var cache = utils.TextureCache;

        //var maskGraphics = new Graphics();
        
        //maskGraphics.beginFill(0x000000);
        //maskGraphics.drawEllipse(25, 25, 100, 175);
        //maskGraphics.endFill();
        
        this.image = new Sprite(cache["assets/images/Lightning.jpg"]);
        this.addChild(this.image);

        //image.mask = maskGraphics;
        //maskGraphics.x = 200;
        //maskGraphics.y = 30;

        //this.addChild(maskGraphics);
    }

    //touchstart
    //pointerdown
    mousedown(e)
    {
        //console.log("Mouse Down On!: " + e.data.global.x + " . " + e.data.global.y);

        var local = e.data.getlocalPosition(this);

        if(this.image.getBounds().contains(e.data.global.x, e.data.global.y))
        {
            console.log("Image Clicked!");
        }
    }

    //touchmove
    //pointermove
    mousemove(e)
    {
        //console.log("Mouse Move!: " + e.data.global.x + " . " + e.data.global.y);
    }

    //touchup
    //pointerup
    mouseup(e)
    {
        //console.log("Mouse Up!: " + e.data.global.x + " . " + e.data.global.y);
    }
}

export default TitleScreen;