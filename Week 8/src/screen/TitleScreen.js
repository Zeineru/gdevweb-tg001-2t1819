import { Container, utils, Sprite, Graphics, ticker } from "pixi.js";

class TitleScreen extends Container
{
    constructor()
    {
        super();
    
        this.elapsedTime = 0;
        this.ticker = ticker.shared;
        this.ticker.add(this.update, this);

        this.drawStar(0, 50, this);
    }
    
    update()
    {
        console.log("UPDATE!" + this.ticker.elapsedMS);
    
        this.elapsedTime += this.ticker.elapsedMS;
    
        if(this.elapsedTime / 1000 > 1)
        {
            this.ticker.remove(this.update, this);
        }
    }

    getColor()
    {
        var color = Math.floor(Math.random() * 14) + 1;

        switch(color)
        {
            case 1:
            {
                return 0X8a878;
            }

            case 2:
            {
                return 0X03028;
            }

            case 3:
            {
               return 0X890f0;
            }

            case 4:
            {
                return 0X040a0;
            }

            case 5:
            {
                return 0X0c068;
            }

            case 6:
            {
                return 0X8a038;
            }

            case 7:
            {
                return 0X890f0;
            }

            case 8:
            {
                return 0X8c850;
            }

            case 9:
            {
                return 0X8c850;
            }

            case 10:
            {
                return 0X8d030;
            }

            case 11:
            {
                return 0X85888;
            }

            case 12:
            {
                return 0X8d8d8;
            }

            case 13:
            {
                return 0X038f8;
            }

            case 14:
            {
                return 0Xe99ac; 
            }

            default:
            {
                return 0XFFFFF;
            }
        }
    }

    drawStar(x, y, container)
    {
        var star = new Graphics;

        star.beginFill(this.getColor());
        star.drawStar(x, y, 5, 100, 50, 0);
        star.endFill();

        container.addChild(star);
    }
}

export default TitleScreen;