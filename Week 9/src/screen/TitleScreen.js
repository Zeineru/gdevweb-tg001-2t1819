import { Container, utils, Sprite, Graphics, ticker } from "pixi.js";
import KeyCodes from "../KeyCodes";
import KeyValues from "../KeyValues";

class TitleScreen extends Container
{
    constructor()
    {
        super();

        window.addEventListener("keydown", this.onKeyDown.bind(this));
        window.addEventListener("keyup", this.onKeyUp.bind(this));
        window.addEventListener("keypress", this.onKeyPress.bind(this));

        this.rectangle = new Graphics;

        this.rectangle.lineStyle(4, 0X78C850);
        this.rectangle.drawRect(50, 50, 100, 50);

        this.addChild(this.rectangle);
    }

    onKeyDown(e)
    {
        console.log("KEYDOWN!: " + e.keyCode);

        switch(e.keyCode)
        {
            case KeyCodes.KeyArrowUp:
            {
                this.rectangle.y -= 20;
            }

            case KeyCodes.KeyArrowDown:
            {
                this.rectangle.y += 20;
            }

            case KeyCodes.KeyArrowRight:
            {
                this.rectangle.x += 20;
            }

            case KeyCodes.KeyArrowLeft:
            {
                this.rectangle.x -= 20;
            }
        }
    }

    onKeyUp(e)
    {
        console.log("KEYUP!: " + e.keyCode);
    }

    onKeyPress(e)
    {
        console.log("KEYPRESS!: " + e.keyCode);                                                     
    }
}

export default TitleScreen;